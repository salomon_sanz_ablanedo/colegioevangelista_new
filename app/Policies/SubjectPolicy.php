<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\User;
use App\Models\Subject;
use Auth, Log;
use App\Policies\BasePolicy;

class SubjectPolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function __construct()
    {

    }

    public function view(User $user){
        return true; //self::hasRole($user,  static::ADMIN);
    }

    public function create(User $user){
        return self::hasRole($user,  static::ADMIN);
    }

    public function update(User $user, Subject $subject){
        return self::hasRole($user, static::ADMIN);
    }

    public function delete(User $user, Subject $subject){
        return self::hasRole($user,  static::ADMIN);
    }
}
