<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\User;
use App\Models\Alumn;
use Auth, Log;
use App\Policies\BasePolicy;

class AlumnPolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function __construct()
    {

    }

    // viewAny, view, create, update, delete, restore, forceDelete
    public function view(User $user, Alumn $alumn){
        return self::hasRole($user, static::ADMIN) || static::isTutor($user, $alumn);
    }

    public function create(User $user){
        return self::hasRole($user, static::ADMIN);
    }

    public function update(User $user, Alumn $alumn){
        return self::hasRole($user, static::ADMIN) || static::isTutor($user, $alumn);
    }

    public function delete(User $user, Alumn $alumn){
        return self::hasRole($user,  static::ADMIN);
    }

    static function isTutor($user, $alumn){
        return $user->userable && !empty($user->userable->tutor_class) && $alumn->classroom()?->id && in_array($alumn->classroom()->id, $user->userable->tutor_class->pluck('id')->toArray());
    }
}
