<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\User;
use App\Models\SubjectArea;
use Auth, Log;
use App\Policies\BasePolicy;

class SubjectAreaPolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function __construct()
    {

    }

    public function create(User $user){
        return self::hasRole($user,  static::ADMIN);
    }

    public function update(User $user, SubjectArea $subject){
        return self::hasRole($user, static::ADMIN);
    }

    public function delete(User $user, SubjectArea $subject){
        return self::hasRole($user,  static::ADMIN);
    }
}
