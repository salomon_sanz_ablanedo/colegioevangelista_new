<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\User;
use App\Models\Promotion;
use Auth, Log;
use App\Policies\BasePolicy;

class PromotionPolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function __construct()
    {

    }

    // viewAny, view, create, update, delete, restore, forceDelete
    public function view(User $user, Promotion $promotion){
        return self::hasRole($user, static::ADMIN);
    }

    public function create(User $user){
        return self::hasRole($user, static::ADMIN);
    }

    public function update(User $user, Promotion $promotion){
        return self::hasRole($user, static::ADMIN);
    }

    public function delete(User $user, Promotion $promotion){
        return false;//self::hasRole($user,  static::ADMIN);
    }
}
