<?php

namespace  App\Policies;
use Auth;
use Illuminate\Support\Arr;

abstract class BasePolicy
{
    //const ADMIN = ['admin','administrator'];
    const ADMIN = ['admin','administrator'];

    static function hasRole($user, $role)
    {
        return $user->hasRole($role);
        // $role = Arr::first($user->roles, function ($value, $key) use($role){
        //     return $value['name'] == $role;
        // });

        //return empty($role) ? false : true;
    }
}