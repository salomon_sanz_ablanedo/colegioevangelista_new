<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use App\Models\Classroom;
use Illuminate\Support\Arr;

class ClassroomsFilter extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value){
        
        return $query->join('alumn_classrooms', function($join)
                    {
                        $join->on('alumn_classrooms.alumn_id', '=', 'alumns.id');
                    })
                    // DO NOT REMOVE! 6-12-21 ¡important to override! If not the previous JOIN overrides with the alumn_classrooms ID
                    ->addSelect('alumns.*') 
                    ->where('alumn_classrooms.classroom_id', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $options = [
            //'Todos' => ''
        ];

        $classes = Classroom::with('grade.cycle')
                        ->orderBy('grade_id','asc')
                        //->orderBy('number','asc')
                        ->get();

        foreach($classes as $class)
            $options[$class->grade->complete_name] = $class->id; 

        return $options;
    }
}
