<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use App\Models\MarkType;

class AlumnMarksByMarkTypeFilter extends Filter
{
    public $component = 'select-filter';

    public $name = 'Curso';

    public function apply(Request $request, $query, $value)
    {
        if (!empty($value))
        {
            return $query
                    ->join('marks', function($join)
                    {
                        $join->on('alumn_marks.mark_id', '=', 'marks.id');
                    })
                    ->join('mark_types', function($join)
                    {
                        $join->on('marks.type_id', '=', 'mark_types.id');
                    })
                    ->where('marks.type_id', $value)
                    ->addSelect(\DB::raw('alumn_marks.id as id, alumn_id, mark_id, marks.type_id, alumn_marks.value, marks.classroom_id'));
        }
        else
            return $query;
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $options = [];
        $types = MarkType::whereIn('id', MarkType::GLOBAL_TYPES)->orderBy('id', 'asc')->get();

        foreach($types as $type)
            $options[$type->name] = $type->id;

        return $options;
    }
}
