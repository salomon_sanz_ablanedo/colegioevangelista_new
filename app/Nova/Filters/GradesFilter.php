<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use App\Models\Grade;
use Illuminate\Support\Arr;

class GradesFilter extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value){
        return $query->where('grade_id', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $options = [
            //'Todas' => ''
        ];

        $grades = Grade::with('cycle')
                        ->where('cycle_id','>','2')
                        ->orderBy('cycle_id','asc')
                        ->orderBy('number','asc')
                        ->get();

        foreach($grades as $grade)
            $options[$grade->complete_name] = $grade->id; 

        return $options;
    }
}
