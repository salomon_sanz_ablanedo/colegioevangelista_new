<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use App\Models\Classroom;
use App\Models\Mark;

class AlumnMarksByClassroomFilter extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    public $name = 'Curso';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
       // return $query->whereIn('mark_id', explode(',',$value));
       //dd($value);
        if (is_array($value))
            return $query->whereIn('mark_id', $value);
        else
            return $query->whereNull('mark_id');
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $options = [];
        $classrooms = Classroom::where('grade_id', '>', 3)->orderBy('grade_id', 'asc')->get();

        foreach($classrooms as $classroom)
        {
            $ids = Mark::where('classroom_id',$classroom->id)->get();

            if (count($ids) > 0)
                $options[$classroom->complete_name] = $ids->pluck('id');
            else
                $options[$classroom->complete_name] = collect([random_int(10000,100000)]);
        }

        return $options;
    }
}
