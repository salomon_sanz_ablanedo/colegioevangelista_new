<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\BelongsTo;

class SubjectValuableType extends Resource
{
    public static $model = 'App\Models\SubjectValuableType';

    public static $title = 'name';

    public static $search = [];

    public static $with = ['subject'];

    public static $displayInNavigation = false;

    public static function label(){
        return __('Categorías de evaluación');
    }

    public static function singularLabel(){
        return __('Categoría de evaluación');
    }

    public function fields(Request $request)
    {
        return [

            BelongsTo::make('Asignatura','subject', \App\Nova\Subject::class),

            Text::make('Nombre','name')
                ->rules('required', 'max:255')
                ->withMeta(['extraAttributes' => [
                    'placeholder' => 'Ej: "Cuaderno"']
            ]),

            Number::make('Porcentaje','percent')
                ->min(1)
                ->max(100)
                ->rules('required', 'max:255')
                ->help(
                    'Valor de 1 a 100, la suma de todas las categorías de evaluación deben sumar 100'
                )

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
