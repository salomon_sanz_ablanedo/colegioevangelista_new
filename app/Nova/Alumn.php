<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasOneThrough;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Panel;
use Laravel\Nova\Fields\Country;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\BelongsTo;
// action
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;
use App\Nova\Actions\DownloadPDFMarks;
use App\Nova\Actions\GenerateExpedient;
use App\Nova\Actions\AddGradeSubjectsToAlumn;
use App\Nova\Actions\CreateRegistrationRequest;
use Hnassr\NovaKeyValue\KeyValue;
use AwesomeNova\Cards\FilterCard;
use App\Nova\Filters\ClassroomsFilter;
use Fourstacks\NovaRepeatableFields\Repeater;

class Alumn extends Resource
{
    public static $group = '';
    public static $model = 'App\Models\Alumn';

    public static $with = ['subject_absences','user','subjects','classrooms','contacts'];

    public static $search = [
        'name', 'surname'
    ];

    public static function label(){
        return __('Alumnos');
    }

    public function title(){
        return $this->name . ' '.$this->surname;
    }

    public function fields(Request $request)
    {
        return [
                //ID::make()->sortable(),

                Avatar::make('foto', 'photo')->disk('public')
                    ->disk('public')
                    ->disableDownload()
                    ->maxWidth(100),

                Text::make('Nombre', 'name')
                    //->rules('required', 'max:255'),
                    ->sortable(),

                Text::make('Apellidos', 'surname')
                    //->rules('required', 'max:255'),
                    ->sortable(),

                Text::make('DNI', 'dni')
                    ->rules('max:10')
                    ->hideFromIndex(),                    
                    //->rules('required', 'max:255'),                    

                HasOneThrough::make('Clase', 'classroom', \App\Nova\Classroom::class),                       

                Number::make('Precio especial (becado)', 'price_with_scholarship')
                    ->min(0)
                    ->displayUsing(function($v){
                        if (!empty($v))
                            return $v.'€';
                        else
                            'no becado';
                    })
                    ->canSee(function ($request) {
                        return $request->user()->hasRole(['admin','administrator']);
                    }),

                Text::make('Email de alumno', 'email_academic')
                    ->rules('max:255')
                    ->nullable()
                    ->help('email de uso académico (deberes, classroom, etc)')
                    ->readonly(function ($request) {
                        return !$request->user()->hasRole(['admin']);
                    }),

                Date::make('F. Nacimiento', 'birthday')
                    ->format('DD MMM Y'),

                Text::make('Lugar de Nacimiento', 'birthplace')
                    ->rules('max:64')
                    ->hideFromIndex(),
                
                Text::make('Nacionalidad', 'nacionality')
                    ->rules('max:64')
                    ->hideFromIndex(),

                Number::make('Nº de hermanos', 'brothers_total')
                    ->hideFromIndex()
                    ->min(0)->max(100)->step(1)
                    ->help('Incluido él/ella'),
                
                Number::make('Lugar que ocupa', 'brothers_index')
                    ->min(0)->max(100)->step(1)
                    ->hideFromIndex(),

                DateTime::make('Creado / actualizado', 'created_at')
                    ->format('DD MMM YYYY')
                    ->onlyOnDetail(),

                DateTime::make('Actualizado', 'updated_at')
                    ->format('DD MMM YYYY')
                    ->onlyOnDetail(),                
                
                Text::make('Ciudad', 'city')
                    //->rules('required', 'max:32')
                    ->sortable(),
                
                Text::make('Dirección', 'address')
                    ->sortable()
                    //->rules('required', 'max:255')
                    ->hideFromIndex(),

                HasMany::make('Contactos', 'contacts', \App\Nova\AlumnContact::class),            

                HasMany::make('Ausencias', 'subject_absences', \App\Nova\SubjectAbsence::class),

                BelongsToMany::make('Asignaturas', 'subjects', \App\Nova\Subject::class),

                HasMany::make('Notas', 'alumn_marks', \App\Nova\AlumnMark::class)
                    ->canSee(function ($request) {
                        return $request->user()->hasRole(['admin','teacher']);
                }),

                HasMany::make('Documentos', 'docs', \App\Nova\AlumnDoc::class)
                    ->canSee(function ($request) {
                        return $request->user()->hasRole(['admin','administrator']);
                }),

                BelongsToMany::make('Curso', 'classrooms', \App\Nova\Classroom::class),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            new FilterCard(new ClassroomsFilter)
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new ClassroomsFilter
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new DownloadExcel,
            new DownloadPDFMarks,
            new AddGradeSubjectsToAlumn,
            new CreateRegistrationRequest,
            new GenerateExpedient                
        ];
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        //if (!Auth::user()->hasRole('admin'))
        //{
            // $teacher = Teacher::with(['tutor_class'])->find(Auth::user()->userable->id);

            // $query->where(function($query) {
            //     // $query->whereIn('marks.classroom_id', $teacher->subjects->pluck('id'));
            //     if (!empty($teacher->tutor_class))
            //         $query->orWhere('alumns.classroom_id', $teacher->tutor_class->id);
            // });    
        //}

        return $query
            ->orderBy('name','asc');
    }        
}
