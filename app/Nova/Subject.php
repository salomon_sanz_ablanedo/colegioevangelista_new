<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Number;
use Mdixon18\Fontawesome\Fontawesome;
use App\Nova\Filters\GradesFilter;
use AwesomeNova\Cards\FilterCard;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class Subject extends Resource
{
    public static $model = 'App\Models\Subject';
    
    public static $with = ['grade','grade.cycle','teachers','subject_valuable_type','marks','parent_area'];
    public static $globallySearchable = false;
    public static $group = '';
    public static $perPageViaRelationship = 15;

    public static $search = [
        'name'
    ];

    public  function title(){
        return $this->grade->number .' '.$this->grade->cycle->short_name . ' - ' .$this->name;
    }

    public static function label(){
        return __('Asignaturas');
    }

    public function fields(Request $request)
    {
        return [

            Fontawesome::make('icono','icon'),

            Text::make('Asignatura','name')
                ->sortable()
                ->rules('required', 'max:255')
                ->displayUsing(function($name){
                    if (empty($this->parent_area))
                        return $name;
                    else
                        return $name .' ('.$this->parent_area->name.')';
                }),

            BelongsTo::make('Curso','grade', \App\Nova\Grade::class)
                ->sortable(),            
            /* ->searchable(), */

            Text::make('Profesor/es', function () {
                return count($this->teachers) == 0 ? '-- sin asignar --' :  $this->teachers->map(function ($teacher) {
                    return $teacher->name . ' ' . $teacher->surname;
                })->join(', ');
            })->onlyOnIndex(),

            Text::make('', function(){
                if (!$this->isEvaluable())
                    return '-- no evaluable --';
            })->onlyOnIndex(),

            Number::make('Periodicidad de Evaluación (meses)','valuable_each_months')                    
                    ->min(1)
                    ->max(12)
                    ->step(1)
                    ->displayUsing(function($value)
                    {
                        if (!$this->isEvaluable())
                            return '-- no evaluable --';

                        switch($value)
                        {
                            case 1: $text = 'mensual'; break;
                            case 2: $text = 'bimestral'; break;
                            case 3: $text = 'trimestral'; break;
                            case 4: $text = 'cuatrimestral'; break;
                            case 6: $text = 'semestral'; break;
                            case 12: $text = 'anual'; break;
                            default:  $text = '--';//$value .' meses';
                        }

                        return mb_convert_case($text, MB_CASE_TITLE, 'UTF-8');

                    })->help('Si la asignatura no es evaluable puedes dejarlo vacío.')
                    ->onlyOnForms(),

            BelongsTo::make('Asignatura padre:', 'parent_area', \App\Nova\Subject::class)
                ->nullable()
                ->onlyOnForms(),
            
            HasMany::make('Exámenes de la asignatura','marks', \App\Nova\Mark::class),

            BelongsToMany::make('Profesores', 'teachers', \App\Nova\Teacher::class)

            //     ->fields(function () {
            //     return [
            //         Text::make('name'),
            //         Text::make('surname'),
            //     ];
            // }),

            //$this->valuableSubjects(),
        ];
    }

    public function cards(Request $request)
    {
        return [
            new FilterCard(new GradesFilter)
        ];
    }

    public function filters(Request $request)
    {
        return [
            new GradesFilter
        ];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [
            new DownloadExcel
        ];
    }

    /* protected function valuableSubjects(){
        
        $field = [];
        
        if ($this->valuable)
            $field[] = HasMany::make('Categorias de evaluación','subject_valuable_type', \App\Nova\SubjectValuableType::class);
        
        return $this->merge($field);
    } */
}
