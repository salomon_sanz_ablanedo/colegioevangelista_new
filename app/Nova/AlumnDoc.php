<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Text;
use Illuminate\Support\Str;
use Laravel\Nova\Panel;
use Laravel\Nova\Fields\File;
use App\Models\Teacher;
use Auth, Storage;

class AlumnDoc extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\AlumnDoc';
    public static $with = ['alumn'];
    public static $displayInNavigation = false;
    public static $globallySearchable = false;
    public static $group = '';
    public static $search = [];
    public static $perPageViaRelationship = 10;

    public  function title(){
        return $this->name;
    }

    public static function singularLabel(){
        return __('Documento del alumno');
    }

    public static function label(){
        return __('Documentos del alumno');
    }

    // public function title(){
    //     return $this->subject->name;
    // }

    public function fields(Request $request)
    {
        return [
            // BelongsTo::make('Asignatura','subject', \App\Nova\Subject::class),
            // BelongsTo::make('Alumno','alumn', \App\Nova\Alumn::class)
            //     ->sortable(),
                //->hideWhenUpdating(),

            BelongsTo::make('Alumno','alumn', \App\Nova\Alumn::class),
            BelongsTo::make('Tipo de documento','docType', \App\Nova\DocType::class),

            Text::make('Nombre','name')
                ->rules('max:135'),

            File::make('Archivo', 'path')
                ->disk('public')
                ->acceptedTypes('image/*|.pdf|.docx|.zip')
                ->storeAs(function (Request $request) {
                    return $this->alumn->getUserDoc(Str::slug($this->name).'.'.pathinfo($request->path->getClientOriginalName(), PATHINFO_EXTENSION));
                })
                ->preview(function ($value, $disk) {
                    return $value
                        ? Storage::disk($disk)->url($value)
                        : null;
                })
               ->withMeta([
                    'extraAttributes' => [
                        'style' => 'max-width:100px;',
                    ]
                ])
                ->onlyOnForms(),
            
            $this->getFieldPreview(250, 150)
                 ->onlyOnIndex(),
            $this->getFieldPreview(650, 500)
                 ->onlyOnDetail()
            //->onlyOnDetail()
            //))
        ];
    }

    public function getFieldPreview($width, $height)
    {
        return Text::make('Archivo', function() use($width, $height)
            {
                $html = '<a href="'.Storage::disk('public')->url($this->path).'" target="_blank">';
                if (Str::endsWith($this->path, ['.pdf','.PDF']))
                    $html .= '<embed src="'.Storage::disk('public')->url($this->path).'" width="'.$width.'" height="'.$height.'"/>';
                else
                    $html .= '<img src="'.Storage::disk('public')->url($this->path).'" style="height:'.$height.'px;"/>';
                return $html.'</a>';
            })
            ->asHtml();
    }

    public function cards(Request $request)
    {
        return [
            //new FilterCard(new EvaluatedMarksFilter)
        ];
    }

    public function filters(Request $request)
    {
        return [
        ];
    }

    public function lenses(Request $request)
    {
        return [
 
        ];
    }

    public function actions(Request $request)
    {
        return [];
    }
}
