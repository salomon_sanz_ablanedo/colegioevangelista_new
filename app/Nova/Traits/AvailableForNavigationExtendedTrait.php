<?php

namespace App\Nova\Traits;

use Illuminate\Support\Arr;

trait AvailableForNavigationExtendedTrait
{
    public static function availableForNavigation(\Illuminate\Http\Request $request):bool{
        if (is_bool(static::$displayInNavigation))
            return static::$displayInNavigation;
        return $request->user()->hasRole(array_merge(['admin'], Arr::wrap(static::$displayInNavigation ?? ['admin'])));
    }
}