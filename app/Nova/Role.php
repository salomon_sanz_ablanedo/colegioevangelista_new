<?php

namespace App\Nova;

use Laravel\Nova\Resource as NovaResource;
use Laravel\Nova\Http\Requests\NovaRequest;
use ChrisWare\NovaBreadcrumbs\Traits\Breadcrumbs;

use Illuminate\Http\Request;
use Vyuldashev\NovaPermission\Role as RoleOriginal;
use App\Nova\Traits\AvailableForNavigationExtendedTrait;

class Role extends RoleOriginal
{
    use AvailableForNavigationExtendedTrait;

    public static $globallySearchable = false;
    
    public static $displayInNavigation = 'admin';
}
