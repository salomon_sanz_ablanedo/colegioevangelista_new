<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Text;
use Illuminate\Support\Str;

class AlumnContact extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\AlumnContact';
    public static $with = ['alumn'];
    public static $displayInNavigation = false;
    public static $globallySearchable = false;
    public static $group = '';

    public static $search = [];

    public  function title(){
        return $this->name .' ('.$this->relation.')';
    }

    public static function sinbularLabel(){
        return __('Contacto');
    }

    public static function label(){
        return __('Contactos');
    }

    // public function title(){
    //     return $this->subject->name;
    // }

    public function fields(Request $request)
    {
        return [
            ID::make(),
            BelongsTo::make('Alumno','alumn', \App\Nova\Alumn::class),            
            Text::make('Nombre', 'name'),       
            Text::make('Email', 'email'),
            Text::make('Teléfono', 'phone'),
            Text::make('Relación', 'relation'),    
            Text::make('Dirección', 'address')->hideFromIndex(),
            Text::make('Nacionalidad', 'nacionality')->hideFromIndex(),    
            Text::make('DNI', 'dni')->hideFromIndex(),    
            Text::make('Estado civil', 'civil_state')->hideFromIndex(),
            Text::make('Ocupación', 'job')->hideFromIndex(),           
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [
 
        ];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    

    

    

    
}
