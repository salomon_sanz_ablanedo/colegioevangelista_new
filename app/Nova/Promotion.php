<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Pdmfc\NovaFields\ActionButton;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Text;
use App\Nova\Actions\EstablishCurrentPromotion;
use App\Scopes\PromotionScope;

class Promotion extends Resource
{
    public static $model = 'App\Models\Promotion';
    
    public static $with = [];
    public static $globallySearchable = false;
    public static $group = '';
    public static $perPageViaRelationship = 15;

    public static $search = false;

    public  function title(){
        return $this->name;
    }

    public static function label(){
        return __('Promociones');
    }

    public function fields(Request $request)
    {
        return [           
            ActionButton::make('Navegar en esta promoción')
                ->text('usar '.$this->name . (PromotionScope::getPromotionId() == $this->id ? ' (actual)': ''))
                ->action(EstablishCurrentPromotion::class, $this->id)
                ->readonly(function () {                    
                    return PromotionScope::getPromotionId() == $this->id;
                }),

            Text::make('nombre', 'name'),
            Date::make('Fecha comienzo', 'date_start'),
            Date::make('Fecha finalización', 'date_finish'),
        ];
    }

    public function cards(Request $request){
        return [];
    }

    public function filters(Request $request){
        return [];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [
            (new EstablishCurrentPromotion())      
                        ->withoutConfirmation()
                        ->onlyOnDetail()
                        ->canSee(function(){
                            return true; // hidde by default
                        })                                              
        ];        
    }
}
