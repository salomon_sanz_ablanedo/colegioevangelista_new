<?php

namespace App\Nova;

use Laravel\Nova\Resource as NovaResource;
use Laravel\Nova\Http\Requests\NovaRequest;
use ChrisWare\NovaBreadcrumbs\Traits\Breadcrumbs;

use Illuminate\Http\Request;
use Vyuldashev\NovaPermission\Permission as PermissionOriginal;
use App\Nova\Traits\AvailableForNavigationExtendedTrait;

class Permission extends PermissionOriginal
{
    use AvailableForNavigationExtendedTrait;

    public static $globallySearchable = false;
    
    public static $displayInNavigation = 'admin';
}
