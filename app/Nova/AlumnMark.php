<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Text;
use Illuminate\Support\Str;
use App\Nova\Filters\AlumnMarksByClassroomFilter;
use App\Nova\Filters\AlumnMarksByMarkTypeFilter;
use AwesomeNova\Cards\FilterCard;
use Laravel\Nova\Panel;
use App\Models\MarkType;
use App\Models\Teacher;
use Auth, Exception;
Use Log;

class AlumnMark extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\AlumnMark';
    public static $with = ['alumn','mark','mark.subject','mark.subject.parent_area'];
    public static $displayInNavigation = ['teacher'];
    public static $globallySearchable = false;
    public static $group = '';

    public static $search = [];

    public  function title(){
        return $this->mark->subject->name .' - ' .$this->mark->name  . ' ('.$this->alumn->name .' '.$this->alumn->surname.')';
    }

    public static function sinbularLabel(){
        return __('Notas del alumno');
    }

    public static function label(){
        return __('Evaluaciones');
    }

    // public function title(){
    //     return $this->subject->name;
    // }

    public function fields(Request $request)
    {
        return [
            // BelongsTo::make('Asignatura','subject', \App\Nova\Subject::class),

            $this->getAlumnField(),            
                //->hideWhenUpdating(),
            $this->getCalificationNameField(),

                //->hideWhenUpdating(),

            $this->getCalification()
                ->sortable(),

            $this->getCommentField(),

            $this->getAreaFieldIfGlobal()
            //))
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            new FilterCard(new AlumnMarksByClassroomFilter),
            new FilterCard(new AlumnMarksByMarkTypeFilter)
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new AlumnMarksByClassroomFilter,
            new AlumnMarksByMarkTypeFilter
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [
 
        ];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        if (Auth::user()->hasRole('teacher') && !Auth::user()->hasRole('admin'))
        {
            $joins = $query->getQuery()->joins;

            if (empty($joins))
            {
                $query->join('marks', function($join){
                    $join->on('alumn_marks.mark_id', '=', 'marks.id');
                })
                ->addSelect(\DB::raw('alumn_marks.id as id, alumn_id, mark_id, alumn_marks.value, marks.classroom_id'));
            }

            $query->where(function($query) {
                $teacher = Teacher::with(['tutor_class','subjects'])->find(Auth::user()->userable->id);

                $query->whereIn('marks.subject_id', $teacher->subjects->pluck('id'));
                if (!empty($teacher->tutor_class))
                    $query->orWhereIn('marks.classroom_id', $teacher->tutor_class->pluck('id'));
            });    
        }

        return $query
            ->orderBy('alumn_id','asc')
            ->orderBy('mark_id','asc');
    }

    public function getAlumnField()
    {
        if (request()->editMode == 'update')
        {
            $field = Text::make('Alumno')
                ->resolveUsing(function(){
                    return $this->alumn->getCompleteName();
                })
                ->readonly();
        }
        else
        {
            $field = BelongsTo::make('Alumno','alumn', \App\Nova\Alumn::class)
                ->sortable();
        }

        return $field;
    }

    public function getCalificationNameField()
    {
        /* if (!$this->mark->subject->isEvaluable())
        {
            return Text::make('Elemento a calificar')
            ->resolveUsing(function(){
                return 'asignatura no evaluable';
            })->readonly();
        } */

        if (request()->editMode == 'update')
        {
            $field = Text::make('Elemento a calificar')
                ->resolveUsing(function(){
                    return $this->mark->subject->name . ' ('.$this->mark->name.')';
                })->readonly();
        }
        else
        {
            $field = BelongsTo::make('Elemento a calificar','mark', \App\Nova\Mark::class)
                ->sortable();
        }

        
        return $field;                    
    }

    public function getCommentField()
    {
        $limit = 135;

        if (isset($this->mark->subject) && isset($this->mark->subject->name) && in_array(strtolower($this->mark->subject->name), ['tutoría','tutoria']))
            $limit = 300;

        if (isset($this->mark->type_id) && in_array($this->mark->type_id, MarkType::GLOBAL_TYPES))
            $help = 'Este comentario saldrá en el boletín de notas, max. '.$limit.' caracteres';
        else
            $help = 'Comentario interno';

        return Text::make('Comentario','comments')
                    ->rules('max:'.$limit)
                    ->displayUsing(function($t){
                        // if index
                        if (request()->resourceId === null)
                            return Str::limit($t, 10);
                        else
                            return $t;
                    })
                    ->help($help);
    }

    public function getAreaFieldIfGlobal()
    {
        return HasMany::make('Aspectos evaluados', 'areas', \App\Nova\AlumnMarkArea::class);
/*
        //dd($this->mark->type_id);
        if (isset($this->mark->type_id) && in_array($this->mark->type_id, MarkType::GLOBAL_TYPES))
        {
            return [
                HasMany::make('Aspectos evaluados', 'areas', \App\Nova\AlumnMarkArea::class)
            ];
        }
        else {
            return [];
        }*/
    }

    public function getCalification()
    {           
        if (empty($this->mark->subject))
        {
            Log::info('no tiene subject: '. $this->id);
            $field = Text::make('Calificación')->resolveUsing(function(){
                return 'no tiene subject';
            })->readonly();
        }
        elseif (!empty($this->mark->subject->parent_area))
        {
            $field = Text::make('Calificación')
                ->withMeta(["placeholder" => 'se evalúa en '.$this->mark->subject->parent_area->name])
                ->displayUsing(function($nota){
                    return 'se evalúa en '.$this->mark->subject->parent_area->name;
                })
                ->readonly()
                ->help('Aunque la nota se establece en la asignatura padre, puedes crear aspectos evaluables que luego saldrán en el boletín de notas (cuaderno, actitud, etc)');
        }
        else if (!$this->mark->subject->isEvaluable())
        {
            $field = Text::make('Calificación')
                ->resolveUsing(function(){
                    return 'asignatura no evaluable';
                })->readonly();
        }
        else 
        {
            $field = Number::make('Calificación','value')
                    ->nullable()
                    ->min(0)
                    ->max(10)
                    ->displayUsing(function($nota){
                        if (empty($nota))
                            return '--';
                        else
                            return $nota;
                    });
        }

        return $field;

    }

    public static function relatableMarks(NovaRequest $request, $query)
    {                
        if (Auth::user()->hasRole('teacher'))
        {
            $teacher = Teacher::with(['tutor_class','subjects'])->find(Auth::user()->userable->id);

            $query->whereIn('subject_id', $teacher->subjects->pluck('id'));            
            if (!empty($teacher->tutor_class))
                $query->orWhereIn('classroom_id', $teacher->tutor_class->pluck('id'));
        }
        
        try{
            $resource = $request->findResourceOrFail();
            return $query->where('classroom_id', $resource->alumn->classroom_id);  
        }      
        catch(Exception $e)
        {
            return $query;

        }        
    }
}
