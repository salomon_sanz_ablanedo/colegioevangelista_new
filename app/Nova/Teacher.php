<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\MorphOne;
use Laravel\Nova\Fields\Avatar;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;


class Teacher extends Resource
{
    public static $model = 'App\Models\Teacher';
    public static $displayInNavigation = 'administrator';
    public static $with = ['subjects','tutor_class','user'];
    public static $group = 'Config';

    //public static $title = 'name';

    public function title(){
        return $this->name . ' '.$this->surname;
    }

    public static function label(){
        return __('Profesores');
    }

    public static function singularLabel(){
        return __('Profesor');
    }

    public static $search = [
        'name','surname'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            //ID::make()->sortable(),

            Avatar::make('foto', 'photo')
                    ->disk('public')
                    ->disableDownload()
                    ->maxWidth(100),
            
            Text::make('Nombre','name')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Apellidos','surname')
                ->sortable()
                ->rules('required', 'max:255'),

            //HasMany::make('Asignaturas vinculadas','subjects', \App\Nova\SubjectTeacher::class),
            BelongsToMany::make('Asignaturas','subjects', \App\Nova\Subject::class)
                ->searchable(),
            
            MorphOne::make('User'),

            //BelongsTo::make('Es tutor en', 'tutor_class', \App\Nova\Classroom::class)->nullable(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [
            new DownloadExcel,
        ];
    }
}
