<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Number;

class AlumnMarkArea extends Resource
{
    public static $model = 'App\Models\AlumnMarkArea';
    public static $with = ['alumn_mark'];
    public static $displayInNavigation = false;
    public static $globallySearchable = false;
    public static $search = [];

    public static function singularLabel(){
        return __('Calificación por aspecto');
    }

    public static function label(){
        return __('Calificaciones por aspecto');
    }

    public function fields(Request $request)
    {
        return [

            BelongsTo::make('Elemento a calificar','alumn_mark', \App\Nova\AlumnMark::class)
                ->hideWhenUpdating(),
            BelongsTo::make('Aspecto evaluado','area', \App\Nova\SubjectArea::class),

            Number::make('Calificación','value')
                ->min(0)
                ->max(10)
                ->step(0.01)
                ->help('Si usas decimales, usa el punto para poner el separador de decimales'),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
          
        ];
    }


    public function filters(Request $request)
    {
        return [
        ];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [];
    }
}
