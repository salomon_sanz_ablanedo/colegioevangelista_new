<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Text;


class SubjectArea extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\SubjectArea';
    public static $with = [];
    public static $displayInNavigation = false;
    public static $search = [];
    public static $group = 'Config';

    public static $title = 'name';

    public static function label(){
        return __('Areas de asignaturas');
    }

    public static function singularLabel(){
        return __('Areas de asignaturas');
    }

    public function fields(Request $request)
    {
        return [
            ID::make('id')->sortable(),
            Text::make('Nombre','name'),
            //BelongsTo::make('Asignatura','subject', \App\Nova\Subject::class),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
