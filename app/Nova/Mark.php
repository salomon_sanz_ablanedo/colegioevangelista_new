<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Select;
use App\Models\Mark as MarkModel;

use Auth;

class Mark extends Resource
{
    
    public static $model = 'App\Models\Mark';
    public static $with = ['subject','classroom','alumn_marks','type'];
    public static $title = 'name';
    public static $group = '';
    public static $displayInNavigation = false;


    public function title(){
        return ($this->name ?? '-undefined name-') . ' (' . ($this->subject->name ?? '-undefined subject name-') . ' - ' .($this->classroom->complete_name ?? '').')';
    }

    public static $search = [];

    public static function label(){
        return __('Puntuables');
    }

    public static function singularLabel(){
        return __('Puntuable');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Nombre', 'name'),
            BelongsTo::make('Clase','classroom', \App\Nova\Classroom::class),
            BelongsTo::make('Asignatura','subject', \App\Nova\Subject::class),
            BelongsTo::make('Tipo de nota','type', \App\Nova\MarkType::class),
            Date::make('Fecha', 'date')
                ->firstDayOfWeek(1)
                ->format('dddd, DD MMM'),
            Select::make('Valor de nota','valuable_type')->options([
                'NORMAL' => 'Normal',
                'RECUPERATION' => 'Recuperación',
                'IMPROVING' => 'Mejorar nota'
            ])->displayUsingLabels(),
            Select::make('Participantes','target')->options([
                'FULL' => 'Toda la clase',
                'SELECTED' => 'Alumnos concretos'
            ])->displayUsingLabels()
              ->help('Si eliges la opción "Alumnos concretos" tendrás que añadir cada alumno de forma manual'),
            //HasMany::make('Participantes','alumn_marks', \App\Nova\AlumnMark::class),
            HasMany::make('Notas','alumn_marks', \App\Nova\AlumnMark::class)
                //  ->fields(function () {
                //  return [
                //      Text::make('value','value')
                //  ];
            //})
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            //new AssignMarkToClassroom
        ];
    }

    public static function relatableAlumns(NovaRequest $request, $query)
    {
        $classroom_id = MarkModel::find($request->resourceId)->classroom_id;
        
        return $query->where('classroom_id', $classroom_id);
    }

    public static function relatableMarkType(NovaRequest $request, $query)
    {
        if (!Auth::user()->hasRole('admin'))
            return $query->where('internal', '0');
        else
            return $query;
    }
}
