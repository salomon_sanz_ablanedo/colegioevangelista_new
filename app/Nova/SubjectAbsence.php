<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Select;
use AwesomeNova\Cards\FilterCard;
use App\Nova\Filters\AbsenceTypes;
use App\Nova\Actions\AbsencesValidation;
use App\Models\Alumn;

class SubjectAbsence extends Resource
{
    public static $displayInNavigation = ['teacher'];
    public static $model = 'App\Models\SubjectAbsence';
    public static $group = '';

    public static $with = [
        'subject','alumn'
    ];

    public static $title = 'date';
    public static $search = [];
    public static $globallySearchable = false;
    public static $searchable = false;


    public static function label(){
        return __('Faltas');
    }

    public static function singularLabel(){
        return __('Ausencia');
    }

    public function fields(Request $request)
    {
        return [
            Date::make('Fecha', 'date'),
            BelongsTo::make('Alumno', 'alumn', \App\Nova\Alumn::class),
            BelongsTo::make('Asignatura', 'subject', \App\Nova\Subject::class),
            Boolean::make('Justificada', 'justified')
                ->hideWhenCreating(),
            Select::make('Tipo','type')->options([
                'ABSENCE' => 'Ausencia',
                'DELAY'   => 'Retraso'
            ])->displayUsingLabels(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            new FilterCard(new AbsenceTypes)
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new AbsenceTypes()
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new AbsencesValidation()
        ];
    }
}
