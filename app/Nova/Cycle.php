<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Select;



class Cycle extends Resource
{
    

    public static $model = 'App\Models\Cycle';

    public static $group = 'Config';
    public static $with = ['grade'];
    public static $globallySearchable = false;
    public static $title = 'name';
    public static $search = [];
    public static $displayInNavigation = ['administrator'];

    public static function label(){
        return __('Ciclos');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [

            Text::make('Nombre', 'name')
                ->rules('required', 'max:255'),

            Text::make('Abreviatura', 'short_name')
                ->rules('required', 'max:255')
                ->hideFromIndex(),

            Select::make('Sistema de evaluación','evaluation_type')->options([
                'COMPETENCIES' => 'Competencias',
                'CREDITS' => 'Créditos',
                'SCORE' => 'Notas'
            ])->displayUsingLabels(),

            Number::make('Precio matrícula', 'registration_price')->displayUsing(function($v){
                return $v.'€';
            }),

            Number::make('Precio enseñanza', 'education_price')->displayUsing(function($v){
                return $v.'€';
            }),

            Number::make('Orden','order')
                ->min(1)->max(99)->step(1),

            HasMany::make('Cursos de '.$this->name,'grade',\App\Nova\Grade::class),

            HasMany::make('Documentos de matriculación','docs',\App\Nova\CycleDoc::class),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    protected static function applyOrderings($query, array $orderings)
    {
        if (empty($orderings)) {
            // This is your default order
            return $query->orderBy('order', 'asc');
        }

        foreach ($orderings as $column => $direction) {
            $query->orderBy($column, $direction);
        }

        return $query;
    }
}
