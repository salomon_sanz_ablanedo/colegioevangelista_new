<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Text;
use App\Nova\Filters\GradesCyclesFilter;
use AwesomeNova\Cards\FilterCard;


class Grade extends Resource
{
    

    public static $with = ['cycle','subjects'];

    public static $model = 'App\Models\Grade';

    public static $group = 'Config';
    public static $globallySearchable = false;
    public static $displayInNavigation = 'administrator';

    public static $title = 'complete_name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [];

    public static function label()
    {
        return __('Cursos');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Number::make('Curso','number')
                ->min(1)->max(12)->step(1)
                ->rules('required'),

            BelongsTo::make('Ciclo','cycle', \App\Nova\Cycle::class),

            HasMany::make('Asignaturas','subjects', \App\Nova\Subject::class)
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            new FilterCard(new GradesCyclesFilter)
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new GradesCyclesFilter
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    protected static function applyOrderings($query, array $orderings)
    {
        if (empty($orderings)) {
            // This is your default order
            $query->orderBy('cycle_id', 'asc');
            $query->orderBy('number', 'asc');
            return $query;
        }

        foreach ($orderings as $column => $direction) {
            $query->orderBy($column, $direction);
        }

        return $query;
    }

    // protected static function applyOrderings($query, array $orderings)
    // {
    //     if (empty($orderings)) {
    //         // This is your default order
    //         return $query
    //             /*->join('cycles', 'grades.cycle_id', '=', 'cycles.id')
    //             ->addSelect('cycles.order as cycle_order')
    //             ->orderBy('cycle_order', 'asc')*/
    //             ->orderBy('cycle_id', 'asc')
    //             ->orderBy('number', 'asc');
    //     }

    //     foreach ($orderings as $column => $direction) {
    //         $query->orderBy($column, $direction);
    //     }

    //     return $query;
    // }
}
