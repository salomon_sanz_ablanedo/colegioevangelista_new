<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\MorphTo;
use Vyuldashev\NovaPermission\PermissionBooleanGroup;
use Vyuldashev\NovaPermission\RoleBooleanGroup;
use KABBOUCHI\NovaImpersonate\Impersonate;

use Auth;


class User extends Resource
{
    

    public static $model = 'App\Models\User';
    public static $group = 'Config';
    public static $title = 'name';
    public static $globallySearchable = false;
    public static $displayInNavigation = 'administrator';


    public static $search = [
        'name', 'email',
    ];

    public static $with = ['userable'];

    public static function label()
    {
        return __('Usuarios');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            //Gravatar::make(),

            Text::make('Nombre', 'name')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),
            
            Impersonate::make($this)->withMeta([
                'hideText'    => true,
                'redirect_to' => 'admin/resources/alumns'
			])->showOnIndex(function () {
                return $this->canImpersonate();
            }),

            Password::make('Password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:8')
                ->updateRules('nullable', 'string', 'min:8'),

            MorphTo::make('Relación','userable')->types([
                 \App\Nova\Alumn::class,
                 \App\Nova\Teacher::class
            ])->nullable(),

            RoleBooleanGroup::make('Roles'),
            PermissionBooleanGroup::make('Permissions'),
            MorphToMany::make('Roles', 'roles', \Vyuldashev\NovaPermission\Role::class),
            MorphToMany::make('Permissions', 'permissions', \Vyuldashev\NovaPermission\Permission::class),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public function canImpersonate()
    {
        return Auth::user()->hasRole('admin');
    }
}
