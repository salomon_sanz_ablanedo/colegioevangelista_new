<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Badge;
use Laravel\Nova\Http\Requests\NovaRequest;
use AwesomeNova\Cards\FilterCard;
use App\Nova\Filters\RegistrationsFilter;
use App\Nova\Actions\ValidateRegistration;
use App\Nova\Actions\RejectRegistration;
use App\Nova\Actions\SendRegistrationEmail;
use Inspheric\Fields\Url;
use Auth;
use Titasgailius\SearchRelations\SearchesRelations;

class Registration extends Resource
{
    use SearchesRelations;

    public static $displayInNavigation = 'administrator';

    public static $model = 'App\Models\Registration';
    public static $with = ['alumn','grade'];
    public static $globallySearchable = true;
    
    public static $search = [];

    public static $searchRelationsGlobally = true;

    public static $searchRelations = [
        'alumn' => ['name', 'surname'],
    ];

    public static $globalSearchRelations = [
         'alumn' => ['name','surname']
    ];

    public function title(){
        return $this->alumn->surname . ', '.$this->alumn->name .' ('.strtolower($this->registration_type).')';
    }

    public static function label(){
        return __('Solicitudes de Matrículación');
    }

    public static function singularLabel(){
        return __('Solicitud de Matriculación');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [

            ID::make()->sortable(),

            BelongsTo::make('Alumno', 'alumn', \App\Nova\Alumn::class),

            BelongsTo::make('Curso', 'grade', \App\Nova\Grade::class)
                ->sortable(),

            Text::make('Link Formulario')
                    ->displayUsing(function(){
                        return route('form_register.form', [$this->public_id]);
                    })
                    //->clickable(true)
                    ->readonly(function(){
                        return true;
                    })
                    ->onlyOnDetail(),

            Select::make('Tipo','registration_type')->options([
                'REGISTRATION' => 'Matrícula',
                'PREREGISTRATION' => 'Pre-Matrícula',
            ])->displayUsing(function($v){
                return array(
                    'REGISTRATION' => 'Matrícula',
                    'PREREGISTRATION' => 'Pre-Matrícula',
                )[$v];
            }),

            Text::make('Email', 'send_to'),

            Text::make('Precio matrícula', 'price'),
            Text::make('Precio asignaturas', 'price_subjects'),

            // info, success, danger and warning; 
            //'SIN RELLENAR','PENDIENTE REVISION','VALIDADA'
            Badge::make('Estado','status')->map([
                'SIN RELLENAR' => 'info',
                'PENDIENTE REVISION' => 'warning',
                'VALIDADA' => 'success'
            ]),

            Select::make('Estado','status')->options([
                'SIN RELLENAR' => 'Pendiente rellenar',
                'PENDIENTE REVISION' => 'En revisión',
                'VALIDADA' => 'Finalizada'
            ])->onlyOnForms()

           
            //Boolean::make('Descripción', 'description'),
        ];
    }

    public function cards(Request $request)
    {
        return [
            new FilterCard(new RegistrationsFilter)
        ];
    }

    public function filters(Request $request)
    {
        return [
            new RegistrationsFilter
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new ValidateRegistration)->showOnIndex(),
            (new SendRegistrationEmail)->showOnIndex(),
            (new RejectRegistration)->showOnIndex()
        ];
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query;
    }
}
