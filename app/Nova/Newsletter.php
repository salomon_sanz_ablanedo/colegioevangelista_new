<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Badge;
use Laravel\Nova\Fields\BooleanGroup;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Http\Requests\NovaRequest;

class Newsletter extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Newsletter';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Asunto','subject'),
            Trix::make('Texto de información', 'html_text')
                ->alwaysShow(),
                //->rows(5),
            BooleanGroup::make('Destinatarios','recipients_groups')
                ->options($this->getRecipientsOptions()),
            Textarea::make('Otros emails','other_emails')
                ->rows(3)
                ->withMeta(['extraAttributes' => [
                    'placeholder' => 'usuario1@email.com otrousuario2@suemail.com etc...']])
                ->help('Puedes poner otras direcciones de email a los que quieras enviar el mensaje también, separados por espacios o coma'),

            Badge::make('Estado','status')->map([
                'PENDING' => 'info',
                'SENT' => 'success',
            ]),

            Number::make('Total destinarios', 'total_recipients')
                ->onlyOnDetail(),
            Text::make('Emails enviados', function(){
                    return '<span style="font-size:10pt;">'.str_replace(' ', '<br/>', $this->computed_recipients).'</span>';
                })
                ->onlyOnDetail()
                ->asHtml(),

            HasMany::make('Adjuntos', 'attachments', \App\Nova\NewsletterAttachment::class),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new \App\Nova\Actions\SendNewsletter()
        ];
    }

    private function getRecipientsOptions()
    {
        $grades = \App\Models\Grade::with('cycle')->get();

        $result = [];

        foreach($grades as $grade)
            $result[$grade->id] = $grade->getCompleteNameAttribute();

        return $result;
    }
}
