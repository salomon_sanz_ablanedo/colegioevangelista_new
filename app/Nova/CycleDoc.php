<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Http\Requests\NovaRequest;
use Auth;

class CycleDoc extends Resource
{
    public static $displayInNavigation = false;

    public static $model = 'App\Models\CycleDoc';

    public static $title = 'name';

    public static $globallySearchable = false;
    
    public static $search = [];

    public static function label(){
        return __('Documento requeridos para matrícula');
    }

    public static function singularLabel(){
        return __('Documentos requeridos para matrícula');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            BelongsTo::make('Ciclo', 'cycle', \App\Nova\Cycle::class),
            BelongsTo::make('Documento', 'docType', \App\Nova\DocType::class),
            Select::make('Tipo','required_type')->options([
                'MANDATORY' => 'obligatorio',
                'OPTIONAL' => 'opcional',
            ])->displayUsingLabels(),

            Trix::make('Texto de información', 'text_info')->alwaysShow(),
            //Boolean::make('Descripción', 'description'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query;
    }
}
