<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Arr;
use App\Models\Registration;
use Laravel\Nova\Fields\Textarea;
use App\Mail\RegistrationRequestRejected;
use Mail;

class RejectRegistration extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $name = 'Solicitar corrección de datos';

    public function handle(ActionFields $fields, Collection $registration)
    {
        foreach($registration as $reg)
        {
            $reg->status='SIN RELLENAR';
            $reg->save();
            Mail::to($reg->send_to)->queue(new RegistrationRequestRejected($reg->alumn, $fields->reason, route('form_register.form', $reg->public_id)));
        }
    }

    public function fields()
    {
        return [
            Textarea::make('Mensaje', 'reason')
                ->rules('required')
        ];
    }
}
