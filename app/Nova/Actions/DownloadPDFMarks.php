<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Boolean;
use PDF, Auth;
use App\Models\MarkType;
use App\Models\SubjectArea;
use App\Models\SubjectAbsence;
use App\Models\Alumn;
use App\Models\AlumnMark;
use ZipArchive;
use Log;
use View;
use App\Services\AlumnService;

class DownloadPDFMarks extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $name = 'Descargar boletín de notas';

    public function handle(ActionFields $fields, Collection $models)
    {
        $ids = $models->pluck('id');
        $output_files = [];

        foreach($ids as $alumn_id)
        {
            $data = AlumnService::getMarksViewData($alumn_id, $fields->mark_type, $fields->date_from, $fields->date_to, $fields->score_type);
            $pages = [ view('reports.marks', $data) ];

            // Consejo orientador: promociona, no promociona, etc
            if (View::exists('reports.marks_extra.grade'.$data['alumn']->classroom->grade->id) && $data['mark_type']->id == MarkType::PROMOTION_FINAL_MARK)
                $pages[] = view('reports.marks_extra.grade'.$data['alumn']->classroom->grade->id, [ 'alumn' => $data['alumn']]);

            $pdf = PDF::loadView('reports.a4layout', ['pages' => $pages])            
                ->setPaper('A4')
                //->setOption('page-size','A4')
                ->setOption('zoom','1.5')
                //->setOption('dpi', 72)
                ->setOption('margin-bottom', 0)
                ->setOption('margin-top', 0)
                ->setOption('margin-left', 0)
                ->setOption('margin-right', 0);
            
            $local_path = 'storage/'.Str::slug($data['alumn']->name. ' ' .$data['alumn']->surname).'.pdf';

            if (file_exists($local_path))
                unlink($local_path);
            $pdf->save(public_path($local_path));
            $output_files[]=$local_path;
        }

        if (count($output_files) == 1)
        {
            return Action::download(url($local_path), basename($local_path));
        }
        else
        {
            $zip_file = 'storage/notas_'.uniqid().'.zip';
            $zip = new ZipArchive();
            $zip->open(public_path($zip_file), ZipArchive::CREATE | ZipArchive::OVERWRITE);

            foreach($output_files as $file)
                $zip->addFile($file, basename($file));

            $zip->close();
            return Action::download(url($zip_file), basename($zip_file));
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Select::make('Evaluación','mark_type')->options([
                '5' => '1ª Evaluación',
                '6' => '2ª Evaluación',
                '7' => '3ª Evaluación',
                '8' => 'Evaluación final'
            ])->rules('required'),

            Date::make('Faltas desde','date_from')
                ->rules('required'),
            
            Date::make('Faltas hasta','date_to')
                ->rules('required'),

            Select::make('Mostrar las notas','score_type')->options([
                'text' => 'En texto',                
                'number' => 'En número',
            ])->rules('required'),

            // Boolean::make('Enviar email a padres','email_to_parents')->help('Asegúrate antes que están rellenados en la ficha'),
            // Boolean::make('Enviar email a mi','email_to_me')->help('se enviará a '.Auth::user()->email)
        ];        
    }
}
