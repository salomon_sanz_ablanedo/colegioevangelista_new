<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Arr;
use App\Models\Alumn;
use App\Models\Registration;
use App\Models\Grade;
use \App\Models\Classroom;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Select;
use App\Mail\RegistrationRequest;
use Mail, Exception, Log;
use App\Mail\GenericHtmlEmail;

class SendNewsletter extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $name = 'Enviar newsletter';

    public function handle(ActionFields $fields, Collection $newsletters)
    {
        $total_recipients = 0;

        foreach($newsletters as $newsletter)
        {
            $recipients = array_unique($this->getEmails($newsletter));
            $newsletter->computed_recipients = implode(' ', $recipients);
            $newsletter->total_recipients = count($recipients);

            if (empty($fields->email_test))
            {
                $newsletter->status = 'SENT';
                $total_recipients += $newsletter->total_recipients;
            }
            else
            {
                $total_recipients += 1;
                $recipients = [$fields->email_test];
            }

            $newsletter->save();

            foreach ($recipients as $email)
                Mail::to($email)->send(new GenericHtmlEmail($newsletter->subject, $newsletter->html_text, $newsletter->attachments));

        }

        if (empty($fields->email_test))
            return Action::message('Se ha enviado '.count($newsletters) .' newsletter/s a ' . $total_recipients .' destinatarios');
        else
            return Action::message('Se ha enviado el newsletter al email de prueba: '.$fields->email_test);
    }

    public function fields()
    {
        return [
            Text::make('Email de prueba', 'email_test')
                        ->help('Si rellenas este campo, se enviará sólo un email de prueba a la dirección indicada')
        ];
    }

    private function getEmails($newsletter)
    {
        $filtered = Arr::where($newsletter->recipients_groups, function ($value, $key) {
            return $value;
        });

        $classrooms = Classroom::whereIn('grade_id', array_keys($filtered))->pluck('id');
        $alumns = Alumn::whereIn('classroom_id', $classrooms)->get();

        $result = [];

        foreach($alumns as $alumn)
        {
            if (!empty($alumn->contacts) && is_array($alumn->contacts))
            {
                if (!empty($c['email']) && $this->isValidEmail($c['email']))
                    $result[] = $c['email'];
            }
        }

        $others = $this->extractEmails($newsletter->other_emails);

        foreach($others as $o)
            $result[] = $o;

        return array_unique($result);
    }

    protected function extractEmails ($text){
        preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", $text, $matches);
        return $matches[0];
    }

    protected function isValidEmail($email){
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

}