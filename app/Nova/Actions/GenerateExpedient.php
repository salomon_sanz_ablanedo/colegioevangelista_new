<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Laravel\Nova\Fields\ActionFields;

use App\Scopes\PromotionScope;
use App\Services\AlumnService;
use PDF;
use App\Models\MarkType;
use App\Models\SubjectArea;
use App\Models\SubjectAbsence;
use App\Models\Alumn;
use App\Models\AlumnMark;
use ZipArchive;
use Log;
use DB;

class GenerateExpedient extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $name = 'Descargar expediente';

    public function handle(ActionFields $fields, Collection $alumns)
    {             
        $output_files = [];
        
        foreach($alumns as $alumn) 
        {
            $pages = [];

            $promotions = DB::table('alumns')
                ->join('alumn_classrooms', 'alumn_classrooms.alumn_id', '=', 'alumns.id')
                ->join('classrooms', 'classrooms.id', '=', 'alumn_classrooms.classroom_id')
                ->join('promotions', 'promotions.id', '=', 'classrooms.promotion_id')
                ->join('grades', 'classrooms.grade_id', '=', 'grades.id')
                ->join('cycles', 'grades.cycle_id', '=', 'cycles.id')
                ->select('promotions.*', 'cycles.id as cycle_id', 'cycles.short_name')
                ->where('alumns.id', $alumn->id)
                ->orderBy('promotions.date_start','asc')
                ->get();

            $cycles = [];
    
            foreach($promotions as $promotion)
            {                
                PromotionScope::setPromotionId($promotion->id);
                $data = AlumnService::getMarksViewData($alumn->id, MarkType::PROMOTION_FINAL_MARK, $promotion->date_start, $promotion->date_finish, 'number');

                if (!isset($cycles[$promotion->cycle_id]))
                {
                    $cycles[$promotion->cycle_id] = [
                        'name'  => $promotion->short_name,
                        'pages' => []
                    ];
                }

                $cycles[$promotion->cycle_id]['pages'][] = view('reports.marks', $data);
            }

            foreach($cycles as $cycle)
            {
                $pdf = PDF::loadView('reports.a4layout', ['pages' => $cycle['pages']])
                        ->setPaper('A4')
                        //->setOption('page-size','A4')
                        ->setOption('zoom','1.5')
                        //->setOption('dpi', 72)
                        ->setOption('margin-bottom', 0)
                        ->setOption('margin-top', 0)
                        ->setOption('margin-left', 0)
                        ->setOption('margin-right', 0);

                $local_path = 'storage/'.Str::slug('Expediente '.$alumn->name. ' ' .$alumn->surname. ' |'. $cycle['name']).'.pdf';

                if (file_exists($local_path))
                    unlink($local_path);

                $pdf->save(public_path($local_path));
                $output_files[]=$local_path;   
            }
        }

        // restore promotion
        PromotionScope::setPromotionId(session()->get('promotion'));

        if (count($output_files) == 1)
        {
            return Action::download(url($local_path), basename($local_path));
        }
        else
        {
            $zip_file = 'storage/notas_'.uniqid().'.zip';
            $zip = new ZipArchive();
            $zip->open(public_path($zip_file), ZipArchive::CREATE | ZipArchive::OVERWRITE);

            foreach($output_files as $file)
                $zip->addFile($file, basename($file));

            $zip->close();
            return Action::download(url($zip_file), basename($zip_file));
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [];        
    }
}
