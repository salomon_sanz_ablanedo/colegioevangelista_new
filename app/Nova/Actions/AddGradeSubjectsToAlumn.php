<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Services\AlumnService;

class AddGradeSubjectsToAlumn extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $name = 'Añadir asignaturas del curso';

    public function handle(ActionFields $fields, Collection $alumns)
    {                
        foreach($alumns as $alumn)
        {
            AlumnService::addSubjects($alumn); 
            AlumnService::createSubjectsMarks($alumn);
        }

        return Action::message('Asignaturas creadas correctamente');
    }

    public function fields(){
        return [];
    }
}
