<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;

class EstablishCurrentPromotion extends Action
{
    use InteractsWithQueue, Queueable;

    public function handle(ActionFields $fields, Collection $models)
    {
        session(['promotion' => $models[0]->id]);

        return Action::redirect('/admin/resources/promotions');
        //return Action::push('/resources/promotions');
            /*'viaResource' => 'users',
            'viaResourceId' => 1,
            'viaRelationship' => 'promotions'
          ]);*/
    }

    public function fields()
    {
        return [];
    }
}
