<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Arr;
use App\Models\Alumn;
use App\Models\AlumnSubject;
use App\Models\AlumnMark;
use App\Models\Registration;
use App\Models\Grade;
use App\Models\MarkType;
use App\Models\Mark;
use App\Models\Promotion;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Select;
use App\Mail\RegistrationRequest;
use Mail;

class CreateRegistrationRequest extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $name = 'Crear solicitud de matriculación y enviar';

    public function handle(ActionFields $fields, Collection $alumns)
    {
        $alumns_without_email = $this->getAlumnsWithoutEmail($alumns);

        if (!empty($alumns_without_email))
        {
            return Action::danger('No se ha podido continuar, todos los alumnos deben tener un email de contacto, los siguientes no tienen: <br><br>- '.
            implode('<br>- ', array_map(function($a){
                return $a->name . ' '.$a->surname;
            }, $alumns_without_email)));    
        }

        foreach($alumns as $alumn)
        {
            $registration = Registration::where('alumn_id', $alumn->id)->first();
            
            if (empty($registration))
            {
                $price = $alumn->price_with_scholarship ?? $alumn->classroom->grade->cycle->registration_price;
                $registration_promotion = 2;
                $previous_promotion = Promotion::find($registration_promotion)->prevPromotion->id;
                $subjects_ids = AlumnSubject::withoutGlobalScope('promotion')
                            ->where('promotion_id', $previous_promotion)
                            ->where('alumn_id', $alumn->id)
                            ->get();

                if (!empty($subjects_ids))
                {
                    $subjects_ids = $subjects_ids->pluck('subject_id');

                    // price failed subjects
                    $marks_ids = \App\Models\Mark::withoutGlobalScope('promotion')
                                    ->ofType(MarkType::PROMOTION_FINAL_MARK)
                                    ->whereIn('subject_id', $subjects_ids)
                                    ->get();

                    if (!empty($marks_ids))
                    {
                        $marks_ids = $marks_ids->pluck('id');

                        $total_marks_failed = AlumnMark::withoutGlobalScope('promotion')
                            ->whereIn('mark_id', $marks_ids)
                            ->where(array(
                                ['alumn_id', '=', $alumn->id],
                                ['value', '<', 5]
                            ))
                            ->count();
                    }
                }

                $grade = Grade::find($fields->grade_id);

                $registration = Registration::create(array(
                    'alumn_id'          => $alumn->id,
                    'grade_id'          => $grade->id,
                    'registration_type' => $fields->registration_type,
                    'promotion_id'      => $registration_promotion,
                    'price'             => $price,
                    'price_subjects'    => ($total_marks_failed ?? 0) * ($grade->cycle->registration_price_failed_subject ?? 0),
                    'status'            => 'SIN RELLENAR',
                    'send_to'           => $alumn->email
                ));
            }

            Mail::to($alumn->email)
                ->queue(new RegistrationRequest($alumn, route('form_register.form', $registration->public_id)));
        }

        return Action::message('Solicitudes de matriculación creadas y enviadas');
    }

    public function fields()
    {
        return [
            //BelongsTo::make('Curso', 'grade', \App\Nova\Grade::class),
            Select::make('Curso de inscripción', 'grade_id')->options($this->getGradesList()),
            Select::make('Tipo de inscripción','registration_type')->options([
                'REGISTRATION' => 'Matrícula',
                'PREREGISTRATION' => 'Pre-Matrícula',
            ]),
        ];
    }

    private function getAlumnsWithoutEmail(&$alumns)
    {
        $errors = [];

        foreach($alumns as &$alumn)
        {
            if (empty($alumn->contacts)){
                $errors[] = $alumn;
                continue;
            }

            $contacts = $alumn->contacts;

            foreach($contacts as $c)
            {
                if (!empty($c['email']))
                {
                    $alumn->email = $c['email'];
                    break;
                }
            }

            if (empty($alumn->email))
                $errors[] = $alumn;
        }

        return $errors;
    }

    private function getGradesList()
    {
        $grades = Grade::all();
        $options = Arr::pluck($grades, 'complete_name', 'id');

        return $options;
    }
}
