<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Arr;
use App\Models\Alumn;
use App\Models\Registration;
use App\Models\Grade;
use App\Models\Classroom;
use App\Models\AlumnClassroom;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Select;
use App\Mail\RegistrationRequestValidated;
use Mail;

class ValidateRegistration extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $name = 'Validar';

    public function handle(ActionFields $fields, Collection $registration)
    {
        foreach($registration as $reg)
        {            
            $reg->status='VALIDADA';
            $reg->save();
            $reg->alumn->classroom_id = Classroom::where('grade_id', $reg->grade_id)
                                                   ->where('promotion_id', $reg->promotion_id)
                                                   ->first()
                                                   ->id;
            $reg->alumn->save();

            AlumnClassroom::insertIgnore(array(
                'alumn_id'     => $reg->alumn->id,
                'promotion_id' => $reg->promotion_id            
            ));

            Mail::to($reg->send_to)->queue(new RegistrationRequestValidated($reg->alumn));
        }
    }

    public function fields()
    {
        
    }
}
