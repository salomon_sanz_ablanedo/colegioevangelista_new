<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Laravel\Nova\Fields\Boolean;
use App\Models\SubjectAbsence;

class AbsencesValidation extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $name = 'Justificar / Injustificar faltas';

    public function handle(ActionFields $fields, Collection $models)
    {
        $ids = $models->pluck('id');
        SubjectAbsence::whereIn('id', $ids)->update(['justified' => $fields->justified]);

        return Action::message('Se han marcado ' . count($ids) . ' faltas como '. ($fields->justified ? 'justificadas' : 'no justificadas'));
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Boolean::make('Justificar','justified')
        ];
    }
}
