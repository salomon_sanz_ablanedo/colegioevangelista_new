<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Arr;
use App\Models\Alumn;
use App\Models\Registration;
use App\Models\Grade;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Select;
use App\Mail\RegistrationRequest;
use Mail;

class SendRegistrationEmail extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $name = 'Enviar email';

    public function handle(ActionFields $fields, Collection $registration)
    {
        foreach($registration as $reg)
        {            
            Mail::to($reg->send_to)->queue(new RegistrationRequest($reg->alumn, route('form_register.form', $reg->public_id)));
        }
    }

    public function fields()
    {
        
    }
}
