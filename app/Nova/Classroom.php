<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;


class Classroom extends Resource
{
    public static $displayInNavigation = 'administrator';
    public static $model = 'App\Models\Classroom';
    public static $title = 'complete_name';
    public static $with = ['grade','tutor','alumns','logs'];
    public static $globallySearchable = false;

    public static $search = [];
    public static $group = 'Config';

    public static function label(){
        return __('Clases');
    }

    public static function singularLabel(){
        return __('Clase');
    }

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            // Text::make('Clase', 'name')
            //     ->sortable()
            //     //->rules('required', 'max:255')
            //     ->nullable(),

            BelongsTo::make('Curso', 'grade', \App\Nova\Grade::class),
            BelongsTo::make('Tutor', 'tutor', \App\Nova\Teacher::class)->nullable(),

            BelongsToMany::make('Alumnos', 'alumns', \App\Nova\Alumn::class),

            //new CommentsPanel(),
            //BelongsToMany::make('Alumno','alumn',\App\Nova\Alumn::class)

                // ->sortable()
                // ->rules('required', 'max:255'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
