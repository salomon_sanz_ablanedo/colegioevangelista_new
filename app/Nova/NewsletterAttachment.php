<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\File;
use Illuminate\Support\Str;

class NewsletterAttachment extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\NewsletterAttachment';
    public static $with = ['newsletter'];
    public static $displayInNavigation = false;
    public static $globallySearchable = false;

    public static $search = [];

    public static function label(){
        return __('Ficheros adjuntos');
    }

    public function title(){
        return $this->name;
    }

    public function fields(Request $request)
    {
        return [
            Text::make('Nombre', 'name'),
            BelongsTo::make('Newsletter','newsletter', \App\Nova\Newsletter::class),
            File::make('Archivo', 'file')
                ->disk('public')
                //->acceptedTypes('image/*|.pdf|.docx|.zip')
                ->storeAs(function (Request $request){
                    return 'newsletters/'.md5($this->id).'_'.Str::slug($this->name) . '.'.pathinfo($request->file->getClientOriginalName(), PATHINFO_EXTENSION);
                })
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
