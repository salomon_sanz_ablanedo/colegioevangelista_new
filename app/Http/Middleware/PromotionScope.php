<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure, Cookie;
use App\Models\Promotion;
use App\Models\Centre;
use Exception;
use Auth;
use App\Scopes\PromotionScope as _PromotionScope;

class PromotionScope
{
    // centre id is not necessary to check, already scoped by centre global scope
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::check())
            return $next($request);
        
        // 1st: Centre
        $centre = Centre::find(Auth::user()->getCurrentCentreId());
        $request->centre = $centre;

        // 2nd: Promotion

        $promotion = null;

        if ($request->has('promotion'))        
            $promotion = Promotion::find($request->get('promotion'));       

        if (session()->has('promotion'))        
            $promotion = Promotion::find(session()->get('promotion'));        

        if (empty($promotion))
            $promotion = Promotion::where('active', 1)->first();

        if (empty($promotion))
            $promotion = Promotion::first();

        if (empty($promotion))
            throw new Exception('No se ha encontrado ninguna promoción para el centro');

        $request->session()->put('promotion', $promotion->id);

        _PromotionScope::setPromotionId($promotion->id);
        
        config(['nova.name' => $centre->name . ' ('.$promotion->name.')']);        

        return $next($request);
    }
}
