<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Models\Alumn;
use App\Models\Grade;
use App\Models\AlumnDoc;
use App\Models\DocType;
use Storage;
use App\Models\Registration;

class FormsController extends Controller
{
    public function getPreRegisterInfo()
    {
        $params = array(
            'title' => 'Pre-Matrícula Curso 20/21 E.S.O',
        );

        return view('forms/preregister_info', $params);
    }

    public function getRegister($public_id)
    {
        setlocale(LC_ALL,"ES");

        $registration = Registration::where('public_id', $public_id)->first();
        $alumn = Alumn::with('classroom.grade.cycle')->find($registration->alumn_id);
        $title = 'Matrícula '.$registration->promotion->name;

        if ($registration->status != 'SIN RELLENAR')
        {
            return view('forms/register', array(
                'registration' => $registration,
                'alumn'        => $alumn,
                'title'        => $title
            ));
        }     

        // $grade = $registration->grade;
        // $cycle = $grade->cycle;
        //dd($current_grade->number+1);
        // $next_grade = Grade::where('cycle_id', $current_grade->cycle_id)
        //     ->where('number', (string) $current_grade->number + 1)
        //     ->first();

        // if (empty($next_grade))
        // {
        //     $next_grade = Grade::where('cycle_id', $current_grade->cycle_id+1)
        //     ->where('number', 1)
        //     ->first();
        // }
        //dd($this->getContacts($alumn));

        $params = array(
            'registration' => $registration,
            'alumn'        => $alumn,
            'contacts'     => $this->getContacts($alumn),
            'title'        => $title
        );

        return view('forms/register', $params);
    }

    public function postFile($public_id, Request $request)
    {
        $registration = Registration::where('public_id', $public_id)->first();
        $alumn = Alumn::find($registration->alumn_id);

        // delete previous documents
        $doc = AlumnDoc::where('alumn_id',$alumn->id)
                ->where('type_id',$request->get('type'))
                ->first();

        if (!empty($doc))
        {
            @unlink(storage_path('app/public/'.$doc->path));
            $doc->delete();
        }

        $doctype = DocType::find($request->get('type'));

        if ($request->hasFile('file'))
        {
            $file = $request->file('file');
            $path = Storage::putFile('public/'.$alumn->getUserDoc(), $file, 'public');
        }
        else
        {
            $file = base64_decode(explode(",", $request->get('file'))[1]);
            $path = $alumn->getUserDoc(md5((string) rand(0, 100000))).'.png';
            Storage::put('public/'.$path, $file);
        }

        AlumnDoc::create(array(
            'alumn_id' => $alumn->id,
            'name'     => $doctype->name .' (inscr. online)',
            'type_id'  => $request->get('type'),
            'path'     => $alumn->getUserDoc(basename($path))
        ));

        return response()
                    ->json(['type_id' => $request->get('type')]);
    }

    public function postSubmit($public_id, Request $request)
    {
        $registration = Registration::where('public_id', $public_id)->first();
        $alumn = Alumn::find($registration->alumn_id);
        
        if ($request->has('birthday_day'))
            $alumn->birthday = $request->get('birthday_year').'-'.$request->get('birthday_month').'-'.$request->get('birthday_day');
        
        if ($request->has('birthplace'))
            $alumn->birthplace = $request->get('birthplace');

        if ($request->has('nacionality'))
            $alumn->nacionality = $request->get('nacionality');

        if ($request->has('address'))
            $alumn->address = $request->get('address');
        
        if ($request->has('brothers_total'))
            $alumn->brothers_total = $request->get('brothers_total');

         if ($request->has('brothers_index'))
            $alumn->brothers_index = $request->get('brothers_index');
        
        $alumn->contacts = $request->get('contact');
        $alumn->save();

        $registration->status = 'PENDIENTE REVISION';
        $registration->save();

        return redirect()->route('form_register.form', $registration->public_id);
    }

    private function getContacts($alumn)
    {
        $contact_fields = \App\Nova\Alumn::getContactFields();
        $alumn_contacts = $alumn->contacts ?? [];
        $contacts = [];

        for($a=0; $a<2-count($alumn_contacts); $a++)
            $alumn_contacts[] = [];

        foreach($alumn_contacts as $contact)
        {
            $new_contact = [];

            foreach($contact_fields as $key=>$val)
            {
                $new_contact[$key] = $val;
                if (isset($contact[$val['name']]))
                    $new_contact[$key]['value'] = $contact[$val['name']];
            }
            $contacts[] = $new_contact;
        }

        return $contacts;
    }
}
