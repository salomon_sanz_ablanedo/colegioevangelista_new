<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Laravel\Nova\Nova;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\InvalidStateException;

class SocialLoginController extends Controller
{
    public function redirectToGoogle()
    {
        return Socialite::driver('google')
            // Displays the G-Suite domain saving the user from typing their whole
            // email address.
            ->with(['hd' => 'colegioevangelista.com'])
            ->redirect();
    }

    public function processGoogleCallback(Request $request)
    {
        try {
            $socialUser = Socialite::driver('google')->user();
        } catch (InvalidStateException $exception) {
            return redirect()->route('nova.login')
                ->withErrors([
                    'email' => [
                        __('Ha fallado la autentificación con Google, por favor inténtalo otra vez.'),
                    ],
                ]);
        }

        // Very Important! Stops anyone with any google accessing Nova!
        // if (! Str::endsWith($socialUser->getEmail(), 'colegioevangelista.com')) {
        //     return redirect()->route('nova.login')
        //         ->withErrors([
        //             'email' => [
        //                 __('Sólo se aceptan direcciones de colegioevangelista.com'),
        //             ],
        //         ]);
        // }

        // $user = User::firstOrCreate(
        //     ['email' => $socialUser->getEmail()],
        //     [
        //         'name' => $socialUser->getName(),
        //         'password' => Str::random(32),
        //     ]
        // );

        $user = User::where('email', $socialUser->getEmail())->first();

        if (empty($user))
        {
            return redirect()->route('nova.login')
                ->withErrors([
                    'email' => [
                        __('No hay ningún usuario identificado con este email'),
                    ],
                ]);
        }

        // $google_client_token = [
        //     'access_token' => $user->token,
        //     'refresh_token' => $user->refreshToken,
        //     'expires_in' => $user->expiresIn
        // ];

        // $client = new Google_Client();
        // $client->setApplicationName("Laravel");
        // $client->setDeveloperKey(env('GOOGLE_SERVER_KEY'));
        // $client->setAccessToken(json_encode($google_client_token));

        // $service = new Google_Service_People($client);

        // $optParams = array('requestMask.includeField' => 'person.phone_numbers,person.names,person.email_addresses');
        // $results = $service->people_connections->listPeopleConnections('people/me',$optParams);

        $this->guard()->login($user);

        return redirect()->intended(Nova::path());
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard(config('nova.guard'));
    }
}