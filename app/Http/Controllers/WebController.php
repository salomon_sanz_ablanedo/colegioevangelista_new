<?php

namespace App\Http\Controllers;

class WebController extends Controller
{
    public function getIndex(){
        return view('index');
    }

    public function getCollaborate(){
        return view('collaborate');
    }

    public function getLegal(){
        return view('legal');
    }

    public function getPrivacy(){
        return view('privacy');
    }

    public function getCookies(){
        return view('cookies');
    }
}
