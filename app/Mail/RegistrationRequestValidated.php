<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegistrationRequestValidated extends Mailable
{
    use Queueable, SerializesModels;

    protected $alumn;

    public function __construct($alumn)
    {
        $this->alumn = $alumn;
    }

    public function build()
    {
        return $this->subject('Matriculación validada ('.$this->alumn->name .' '.$this->alumn->surname.')')
                    ->from('administracion@colegioevangelista.com')
                    ->view('emails.email_html')
                    ->with([
                        'content' => '
                        Querido padre/madre/tutor: <br/><br/>
                        
                        Le confirmamos que hemos validado la solicitud de matriculación del/a alumno/a '.$this->alumn->name .' '.$this->alumn->surname.'.<br/><br/>

                        Un saludo
                        '
                    ]);
    }
}
