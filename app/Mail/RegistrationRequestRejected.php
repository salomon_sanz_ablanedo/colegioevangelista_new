<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegistrationRequestRejected extends Mailable
{
    use Queueable, SerializesModels;

    protected $alumn;
    protected $reason;
    protected $url;

    public function __construct($alumn, $reason, $url)
    {
        $this->alumn = $alumn;
        $this->reason = $reason;
        $this->url = $url;
    }

    public function build()
    {
        return $this->subject('Dato de matrículación incompleto o incorrecto')
                    ->from('administracion@colegioevangelista.com')
                    ->view('emails.email_html')
                    ->with([
                        'content' => '
                        Querido padre/madre/tutor: <br/><br/>
                        
                        Hemos encontrado los siguiente problemas en la solicitud de matriculación del/a alumno/a '.$this->alumn->name .' '.$this->alumn->surname.':<br/><br/>

                        <i>&quot;'.nl2br($this->reason).'&quot;</i>
                        <br/><br/>
                        Le rogamos que realize la modificación solicitada y vuelva a enviar el formulario.<br/>
                        Puede usar el siguiente enlace para realizar la modificación.<br/><br/>
                        <a href="'.$this->url.'">'.$this->url.'</a>
                        <br><br/>
                        Un saludo
                        '
                    ]);
    }
}
