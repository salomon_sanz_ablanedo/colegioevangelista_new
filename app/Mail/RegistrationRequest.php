<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegistrationRequest extends Mailable
{
    use Queueable, SerializesModels;

    protected $alumn, $url;

    public function __construct($alumn, $url)
    {
        $this->alumn = $alumn;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Enlace para matriculación online de '.$this->alumn->name .' '.$this->alumn->surname)
                    ->from('administracion@colegioevangelista.com')
                    ->view('emails.email_html')
                    ->with([
                        'content' => '
                        Querido padre/madre/tutor: <br/><br/>
                        
                        A continuación le mostramos el enlace para que pueda realizar la matriculación online de '.$this->alumn->name .' '.$this->alumn->surname.'.<br/><br/>

                        <a href="'.$this->url.'">'.$this->url.'</a>
                        <br/><br/>

                        Un saludo
                        '
                    ]);
    }
}
