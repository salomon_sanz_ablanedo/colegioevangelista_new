<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Storage;

class GenericHtmlEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject, $body, $at;

    public function __construct($subject, $body, $at)
    {
        $this->subject = $subject;
        $this->body = $body;
        $this->at = $at;

        if (!empty($at))
        {
            $this->body .= '<br/>---------------------------<br/>Ficheros adjuntos (pincha para descargar):<br>
            <ul>';

            foreach($at as $a)
                $this->body .= '<li><a href="'.asset(Storage::url($a->file)).'" download>'.$a->name.'</a></li>';

            $this->body .= '</ul>';
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        \Log::debug(view('emails.email_html')
                    ->with([
                        'content' => $this->body
                    ]));
        return $this->subject($this->subject)
                    ->from('administracion@colegioevangelista.com')
                    ->view('emails.email_html')
                    ->with([
                        'content' => $this->body
                    ]);
    }
}
