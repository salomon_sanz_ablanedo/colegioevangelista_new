<?php

namespace App\Providers;

use Laravel\Nova\Nova;
use Laravel\Nova\Cards\Help;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Laravel\Nova\NovaApplicationServiceProvider;

// models to observe
use App\Models\Alumn;
use App\Models\AlumnMark;
use App\Models\AlumnSubject;
use App\Models\Classroom;
use App\Models\Registration;
use App\Models\Subject;
use App\Models\SubjectAbsence;
use App\Models\Teacher;

//observers
use App\Observers\AlumnObserver;
use App\Observers\AlumnMarkObserver;
use App\Observers\AlumnSubjectObserver;
use App\Observers\ClassroomObserver;
use App\Observers\RegistrationObserver;
use App\Observers\SubjectObserver;
use App\Observers\SubjectAbsenceObserver;
use App\Observers\TeacherObserver;

use App\Http\Controllers\SocialLoginController;
use Illuminate\Support\Facades\Route;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Nova::serving(function () {
            Alumn::observe(AlumnObserver::class);
            AlumnMark::observe(AlumnMarkObserver::class);
            AlumnSubject::observe(AlumnSubjectObserver::class);
            Classroom::observe(ClassroomObserver::class);
            Registration::observe(RegistrationObserver::class);
            Subject::observe(SubjectObserver::class);
            SubjectAbsence::observe(SubjectAbsenceObserver::class);
            Teacher::observe(TeacherObserver::class);
        });
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
                ->withAuthenticationRoutes()
                ->withPasswordResetRoutes()
                ->register();

        Route::domain(config('nova.domain', null))
            ->middleware(['web'])
            ->as('nova.')
            ->prefix(Nova::path())
            ->group(function () {
                Route::get('/login', 'Laravel\Nova\Http\Controllers\LoginController@showLoginForm');
                Route::get('/login/google', SocialLoginController::class . '@redirectToGoogle')
                    ->name('login.google');
                Route::get('/google/callback', SocialLoginController::class . '@processGoogleCallback')
                    ->name('login.callback');
            });

        Route::namespace('Laravel\Nova\Http\Controllers')
            ->domain(config('nova.domain', null))
            ->middleware(config('nova.middleware', []))
            ->as('nova.')
            ->prefix(Nova::path())
            ->group(function () {
                Route::get('/logout', 'LoginController@logout')->name('logout');
            });
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            return true;
            return in_array($user->email, [
                //
            ]);
        });
    }

    protected function cards()
    {
        return [
            //new Help,
        ];
    }

    /**
     * Get the extra dashboards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function dashboards()
    {
        return [
        ];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [            
            \ChrisWare\NovaBreadcrumbs\NovaBreadcrumbs::make(),            
            \Vyuldashev\NovaPermission\NovaPermissionTool::make()
                ->roleResource(\App\Nova\Role::class)
                ->permissionResource(\App\Nova\Permission::class),
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected function resources()
    {
        Nova::resourcesIn(app_path('Nova'));
    }
}
