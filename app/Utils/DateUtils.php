<?php

namespace App\Utils;

class DateUtils
{
    static public function monthName($index)
    {
        return array(
            '',
            'Enero',
            'Febrero',
            'Marzo',
            'Abril',
            'Mayo',
            'Junio',
            'Julio',
            'Agosto',
            'Septiembre',
            'Octubre',
            'Noviembre',
            'Diciembre'
        )[$index];
    }
}