<?php

namespace App\Observers;

use App\Models\Registration;

class RegistrationObserver
{
    public function creating(Registration $registration)
    {
        if (empty($registration->public_id))
            $registration->public_id = md5(mt_rand(0,10000000));

        if (empty($registration->promotion_id))
            $registration->promotion_id = request()->promotion;
    }
}