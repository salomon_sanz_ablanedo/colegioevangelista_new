<?php

namespace App\Observers;

use App\Scopes\PromotionScope;

trait _PromotionTrait
{
     public function creating($model){
        $model->promotion_id = PromotionScope::getPromotionId();
    }

    public function updating($model){
        $model->promotion_id = PromotionScope::getPromotionId();
    }
}
