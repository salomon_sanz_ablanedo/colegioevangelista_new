<?php

namespace App\Observers;

use App\Models\Subject;
use App\Models\AlumnSubject;
use LogicException;
use App\Observers\_PromotionTrait;

class SubjectObserver
{
    use _PromotionTrait;

    public function created(Subject $subject)
    {
       /*  $grade = with(new \App\Models\Classroom)->with('alumns')->where('grade_id', $subject->grade_id)->first();
        
        if (empty($grade))
            return;
        
        $alumns = $grade->alumns()->get() ?? [];

        $inserts = [];

        foreach($alumns as &$alumn)
        {
            $alumn->createMandatorySubjects();
        } */
    }

    public function updated(Subject $subject)
    {
        // TODO: if alumn has this subject throw error
        $count = AlumnSubject::where('subject_id',$subject->id)->count();

        if ($subject->isDirty('grade_id') && $count > 0)
            throw new LogicException('No se puede cambiar de curso la asignatura por que ya está vinculado a '.$count.' alumnos. Es mejor crear otra asignatura, o quitar esta asignatura a los alumnos que la tengan.');
    }

    public function deleted(Subject $subject)
    {
        //
    }

    public function restored(Subject $subject)
    {
        //
    }

    public function forceDeleted(Subject $subject)
    {
        //
    }
}
