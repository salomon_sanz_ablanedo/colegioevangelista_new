<?php

namespace App\Observers;

use App\Models\Teacher;
use App\Models\AlumnSubject;
use LogicException;
use Log;

class TeacherObserver
{
    public function creating(Teacher $teacher)
    {
        $teacher->centre_id = 1;
    }

    public function updating(Teacher $teacher)
    {
        Log::info('TeacherObserver:updating');
        $teacher->centre_id = 1;
    }
}
