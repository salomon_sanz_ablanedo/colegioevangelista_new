<?php

namespace App\Observers;

use App\Models\Alumn;
use App\Models\AlumnSubject;
use App\Models\AlumnClassroom;
use App\Services\AlumnService;

class AlumnObserver
{
    public $afterCommit = true;

    public function created(Alumn $alumn)
    {
        if (empty($alumn->classroom_id))
            return;
        
        AlumnService::addSubjects($alumn);
        $this->registerClassroom($alumn);        
    }

    public function updated(Alumn $alumn)
    {
        if (!$alumn->wasChanged('classroom_id'))
            return;
        
        AlumnService::addSubjects($alumn);
        $this->registerClassroom($alumn);        
    }

    private function registerClassroom($alumn)
    {
        if (!empty($alumn->classroom_id))
            AlumnService::registerClassroom($alumn, $alumn->classroom);
        else
            AlumnService::unregisterClassroom($alumn);
    }
}
