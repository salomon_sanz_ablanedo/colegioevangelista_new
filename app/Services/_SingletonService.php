<?php

namespace App\Services;


class _SingletonService
{
    private static $instance;

	public static function instance()
    {
     	if (!self::$instance instanceof self)
        	self::$instance = new static;
		
		return self::$instance;
    }
}