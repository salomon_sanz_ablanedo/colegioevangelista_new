<?php

namespace App\Services;

use App\Models\Alumn;
use App\Models\AlumnMark;
use App\Models\AlumnClassroom;
use App\Models\Classroom;
use App\Models\Subject;
use App\Models\MarkType;
use App\Models\Mark;
use App\Scopes\PromotionScope;
use App\Services\_SingletonService;
use App\Models\SubjectAbsence;
use App\Models\SubjectArea;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AlumnService extends _SingletonService
{
    static public function registerClassroom(Alumn $alumn, Classroom $classroom){                
        return AlumnClassroom::sync([$classroom->id]);
    }

    static public function unregisterClassroom(Alumn $alumn)
    {
        return $alumn->classroom()->detach();
    }

    static public function addSubjects(Alumn $alumn)
    {
        $subjects = Subject::where('grade_id', $alumn->classroom()->grade_id);
        $sub_ids = $subjects->pluck('id');        
        $alumn->subjects()->syncWithPivotValues($sub_ids, ['promotion_id' => PromotionScope::getPromotionId()]);
    }

    static public function createSubjectsMarks(Alumn $alumn)
    {
        foreach($alumn->subjects as $subject)
        {
            foreach(MarkType::GLOBAL_TYPES as $type)
            {
                $mark = Mark::ofType($type)->where('subject_id', $subject->id)->first();

                if (empty($mark))
                {
                    if (empty($mark_types[$type]))
                        $mark_types[$type] = MarkType::find($type)->name;

                    $mark = Mark::create(array(
                        'subject_id' => $subject->id,
                        'classroom_id' => $alumn->classroom()->id,
                        'type_id' => $type,
                        'name' => $mark_types[$type]
                    ));
                }

                $mark_result = $mark->alumn_marks($alumn->id)->first();

                if (empty($mark_result))
                {
                    AlumnMark::create(array(
                        'alumn_id'     => $alumn->id,
                        'mark_id'      => $mark->id                        
                    ));
                }                
            }
        }
    }    

    static public function getMarksView($alumn_id, $mark_type, $date_from, $date_to, $score_type){
        return View::make('reports.marks', static::getMarksViewData($alumn_id, $mark_type, $date_from, $date_to, $score_type));
    }

    static public function getMarksViewData($alumn_id, $mark_type, $date_from, $date_to, $score_type)
    {
        $simbols = array(
            9 => 'SB',
            7 => 'NT',
            6 => 'BI',
            5 => 'SU',
            0 => 'IN' 
        );

        $alumn = Alumn::with(['subjects','classrooms','classrooms.grade','classrooms.tutor','classrooms.promotion'])->find($alumn_id);

        $data = [];
        $data['all_areas']    = Arr::pluck(SubjectArea::all()->toArray(), 'name', 'id');
        $data['mark_type'] = MarkType::find($mark_type);
        $alumn->classroom = $alumn->classroom();
        $data['alumn']        = $alumn;        

        $areas_id = [];        

        $subjects_marks = [];
        $comments = '';
        $subjects = $alumn->subjects ?? [];

        foreach($subjects as $s)
        {
            $subject = (object) array(
                'id'      => $s->id,
                'name'    => $s->name,
                'marks'   => [],
                'comments' => null,
                'parent_area_id' => $s->parent_area_id,
                'valuable' => $s->valuable_each_months,
                'areas'   => []
            );

            // list all global subjects of a custom signature
            foreach(MarkType::GLOBAL_TYPES as $type)
            {
                // if ($type != $mark_type)
                // {
                //     $subject->marks[] = ''; //NE
                //     continue;
                // }
                // else
                // {     

                    $mark = \App\Models\Mark::ofType($type)
                        ->where(array(
                            'subject_id' => $subject->id,
                            'classroom_id' => $alumn->classroom()->id
                        ))
                        ->first();

                    if (empty($mark))
                    {
                        $subject->marks[] = '';
                        continue;
                    }

                    $mark_result = AlumnMark::with('areas')
                        ->where(array(
                            'alumn_id' => $alumn->id,
                            'mark_id' => $mark->id,
                        ))
                        ->first();

                    if (in_array($subject->name, ['Tutoría','Tutoria']) && $type == $mark_type)
                    {
                        if (!empty($mark_result))
                            $comments = $mark_result->comments;
                        
                        continue;
                    }

                    if (empty($mark_result))
                    {
                        if ($type > $mark_type)
                            $subject->marks[] = '';
                        else
                            $subject->marks[] = 'NE';
                    }
                    else
                    {    
                        if ($mark_type == $type)
                        {
                            $subject->comments = $mark_result->comments;

                            if ($s->show_areas_report)
                            {
                                foreach($mark_result->areas as $area)
                                {
                                    if (!empty($area))
                                    {
                                        $areas_id[]=$area->subject_area_id;
                                        $subject->areas[$area->subject_area_id] = $area->value;
                                    }
                                }
                            }
                        } 

                        if (empty($mark_result->value))
                        {
                            if ($type > $mark_type)
                                $subject->marks[] = '';
                            else
                                $subject->marks[] = 'NE';
                        }
                        else
                        {
                            $subject->marks[] = Arr::first($simbols, function ($value, $key) use ($mark_result, $score_type){
                                return $mark_result->value >= $key;
                            }) . ($score_type == 'number' ? ' - '. $mark_result->value : '');
                        }
                    }                         
                //}
            }

            if (!in_array($subject->name, ['Tutoría','Tutoria']))
                $subjects_marks[] = $subject;
        }

        $absences = array(
            'absences' => [],
            'delay'    => []
        );

        $absences['absences'][] = SubjectAbsence::where('alumn_id', $alumn->id)
                    ->where('type','ABSENCE')
                    ->where('justified','0')
                    ->whereBetween('date', [$date_from, $date_to])
                    ->count();
        
        $absences['absences'][] = SubjectAbsence::where('alumn_id', $alumn->id)
                    ->where('type','ABSENCE')
                    ->where('justified','1')
                    ->whereBetween('date', [$date_from, $date_to])
                    ->count();

        $absences['delay'][] = SubjectAbsence::where('alumn_id', $alumn->id)
                    ->where('type','DELAY')
                    ->where('justified','0')
                    ->whereBetween('date', [$date_from, $date_to])
                    ->count();

        $absences['delay'][] = SubjectAbsence::where('alumn_id', $alumn->id)
                    ->where('type','DELAY')
                    ->where('justified','1')
                    ->whereBetween('date', [$date_from, $date_to])
                    ->count();
         
        $data['comments'] = $comments;
        $data['subjects'] = $subjects_marks;
        $data['areas']    = array_unique($areas_id);
        $data['absences'] = $absences;        
        //dd($data['mark_type']->id . ' ' . \App\Models\MarkType::PROMOTION_FINAL_MARK);

        $data['alumn_header_data'] = array(
            'alumn'  => $alumn->surname .', ' .$alumn->name,
            'grade'  => $alumn->classroom->grade->number .' º de '. $alumn->classroom->grade->cycle->name,
            'tutor'  => $alumn->classroom->tutor->name . ' '. $alumn->classroom->tutor->surname,
            'period' => $data['mark_type']->name .', '. $alumn->classroom->promotion->name,            
        );

        return $data;              
    }
}