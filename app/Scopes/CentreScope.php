<?php

namespace App\Scopes;
use \Illuminate\Database\Eloquent\Builder;
use Auth;

trait CentreScope
{
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('centre', function (Builder $builder){
            if (Auth::check()) // if not, when logout causes error
                $builder->where((new static)->getTable().'.centre_id', Auth::user()->getCurrentCentreId());
        });
    }
}