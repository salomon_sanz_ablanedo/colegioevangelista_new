<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class PromotionScope implements Scope
{
    static private $promotion_id;
    
    static public function setPromotionId($promotion_id){
        self::$promotion_id = $promotion_id;
    }

    static public function getPromotionId(){
        return self::$promotion_id;
    }

    public function apply(Builder $builder, Model $model)
    {
        $builder->where($model->getTable().'.promotion_id', static::$promotion_id);
    }
}