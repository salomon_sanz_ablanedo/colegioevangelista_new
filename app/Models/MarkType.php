<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarkType extends Model
{
    //
    protected $id;
    protected $name;
    protected $description;
    protected $internal;
    const GLOBAL_TYPES = [5,6,7,8];
    const PROMOTION_FINAL_MARK = 8;
}
