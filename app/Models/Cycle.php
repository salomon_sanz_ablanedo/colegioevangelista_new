<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cycle extends Model
{
    //
    protected $id;
    protected $name;
    protected $short_name;
    protected $evaluation_type;
    protected $registration_price;
    protected $registration_price_failed_subject;
    protected $education_price;
    protected $order;
    protected $centre;
    protected $parental_control;

    public function grade(){
        return $this->hasMany('App\Models\Grade', 'cycle_id');
    }

    public function docs(){
        return $this->hasMany('App\Models\CycleDoc', 'cycle_id');
    }
}
