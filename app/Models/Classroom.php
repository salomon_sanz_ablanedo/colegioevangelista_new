<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use KirschbaumDevelopment\Novacomments\Commentable;
//use KirschbaumDevelopment\NovaComments\Models\Comment;
use App\Scopes\PromotionScope;

class Classroom extends Model
{
    //use Commentable;    

    protected
        $id,
        $name,
        $alias,
        $grade_id,
        $tutor_teacher_id;

    //protected $appends = ['complete_name'];

    // public function comments(){
    //     return $this->morphMany(Comment::class, 'commentable');
    // }
    
    public function grade(){
        return $this->belongsTo(\App\Models\Grade::class,'grade_id');
    }

    public function alumns(){
        // return $this->belongsToMany(\App\Models\Alumn::class);
        //return $this->belongsToMany(\App\Models\Alumn::class, 'classroom_id');
        return $this->belongsToMany(\App\Models\Alumn::class, 'alumn_classrooms', 'classroom_id','alumn_id');
    }

    public function tutor(){
        return $this->belongsTo('App\Models\Teacher','tutor_teacher_id');
    }

    public function promotion(){
        return $this->belongsTo('App\Models\Promotion','promotion_id');
    }

    public function logs(){
        // return $this->belongsToMany(\App\Models\Alumn::class);
        return $this->hasMany(\App\Models\ClassroomLog::class, 'classroom_id');
    }

    public function getCompleteNameAttribute()
    {
        $name = $this->grade['complete_name'];
        $subname = null;

        if (isset($this->attributes['name']))
            $subname .= $this->attributes['name'];

        if (isset($this->alias))
        {
            if (isset($subname))
                $subname .= ' ('.$this->attributes['alias'].')';
            else
                $subname .= ' '.$this->attributes['alias'];
        }

        if (isset($subname))
            $name .= ' - ' . $subname;

        return $name;
    }

    protected static function booted(){
        static::addGlobalScope(new PromotionScope);
    }    
}