<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubjectArea extends Model
{
    protected
        $id,
        $name,
        $subject_id;

    public function subject(){
        return $this->hasOne('App\Models\Subject', 'id','subject_id');
    }
}
