<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CycleDoc extends Model
{
    //
    protected $id;
    protected $cycle_id;
    protected $doc_type_id;
    protected $required_type;
    protected $text_info;

    public function docType(){
        return $this->belongsTo('App\Models\DocType', 'doc_type_id');
    }

    public function cycle(){
        return $this->belongsTo('App\Models\Cycle', 'cycle_id');
    }
}
