<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsletterAttachment extends Model
{
    protected $id;
    protected $name;
    protected $newsletter_id;
    protected $file;
    public $timestamps = false;
    //protected $type;

      protected $fillable = [
        'id','name','newsletter_id','file'
    ];

    public function newsletter(){
        return $this->belongsTo('App\Models\Newsletter', 'newsletter_id');
    }
}
