<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use App\Scopes\PromotionScope;


class AlumnSubject extends Model
{    
    protected $id;
    protected $alumn_id;
    protected $subject_id;
    protected $promotion_id;
    /*
    public function subject(){
        return $this->belongsTo(App\Models\Subject::class, 'subject_id');
    }

    public function alumn(){
        return $this->belongsTo(App\Models\Alumn::class, 'alumn_id');
            //  return $this->belongsTo(App\Models\Alumn::class)
            //     ->using(ChatRoomUser::class)
            //     ->withPivot('alumn_id');
    }
    */
    // public static function boot() 
    // {
    //     static::creating(function ($model) {
    //         \Log::info('creating alumnsubject');
    //     });

    //     parent::boot();
    // }
    
    protected static function booted(){
        static::addGlobalScope(new PromotionScope);
    }
}
