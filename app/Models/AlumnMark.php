<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\AlumnSubject;
use App\Scopes\PromotionScope;
use Exception;

class AlumnMark extends Model
{        
    protected $id;
    protected $alumn_id;
    protected $mark_id;
    protected $value;
    protected $promotion_id;
    //protected $type;
    protected $comments;

    protected $fillable = [
        'alumn_id','mark_id'
    ];

    public function mark(){
        return $this->belongsTo('App\Models\Mark','mark_id');
    }

    public function alumn(){
        return $this->belongsTo('App\Models\Alumn', 'alumn_id');
    }

    public function promotion(){
        return $this->belongsTo('App\Models\Promotion', 'promotion_id');
    }

    // public function subjects(){
    //     return $this->belongsToMany('App\Models\Subject', 'alumn_subjects', 'alumn_id','subject_id');
    // }
    public function areas(){
        return $this->hasMany('App\Models\AlumnMarkArea','alumn_mark_id','id', array(
            'value'
        ));
    }

    protected static function booted(){
        static::addGlobalScope(new PromotionScope);
    }    
}
