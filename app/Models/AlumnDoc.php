<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\AlumnSubject;

use Exception;

class AlumnDoc extends Model
{
    protected $id;
    protected $alumn_id;
    protected $name;
    protected $type_id;
    protected $path;

    protected $fillable = [
        'alumn_id','name','type_id','path'
    ];

    public function alumn(){
        return $this->belongsTo('App\Models\Alumn', 'alumn_id');
    }

    public function docType(){
        return $this->belongsTo('App\Models\DocType', 'type_id');
    }
}
