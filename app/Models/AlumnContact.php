<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Exception;

class AlumnContact extends Model
{
    protected $id;
    protected $alumn_id;
    protected $name;       
    protected $email;
    protected $phone;
    protected $address;
    protected $relation;    
    protected $nacionality;    
    protected $dni;    
    protected $civil_state;
    protected $job;    

    public function alumn(){
        return $this->belongsTo('App\Models\Alumn');             
    }    
}
