<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\PromotionScope;


class Subject extends Model
{
    protected 
        $id,
        $name,
        $grade_id,
        $promotion_id,
        $valuable_each_months,
        $mandatory,
        $parent_area_id,
        $show_areas_report;

    public function grade(){
        return $this->belongsTo('App\Models\Grade');
    }

    public function parent_area(){
        return $this->belongsTo('App\Models\Subject');
    }

    public function teachers(){
        return $this->belongsToMany(\App\Models\Teacher::class, 'subject_teachers', 'subject_id', 'teacher_id');
    }

    public function subject_valuable_type(){
        return $this->hasMany('App\Models\SubjectValuableType', 'subject_id');
    }

    // public function subject_alumns(){
    //     return $this->hasMany('App\Models\SubjectAlumn','subject_id');
    // }

    public function marks(){
        return $this->hasMany(\App\Models\Mark::class, 'subject_id');
    }

    public function isEvaluable(){
        return !empty($this->attributes['valuable_each_months']);
    }

    protected static function booted(){
        static::addGlobalScope(new PromotionScope);
    }    
}
