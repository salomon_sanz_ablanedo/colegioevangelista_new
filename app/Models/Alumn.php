<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\PromotionScope;
use Exception;

class Alumn extends Model
{
    protected $id;
    protected $name;
    protected $surname;
    protected $email_academic;
    protected $dni;
    protected $birthday;
    protected $birthplace;
    protected $nacionality;
    protected $brothers_total;
    protected $brothers_index;
    protected $classroom_id;
    protected $address;
    protected $city;
    //protected $postal_code;
    //protected $country;
    protected $photo;    
    protected $price_with_scholarship;

    // no borrar, usado de forma temporal para mandar emails
    protected $email;

    protected $dates = [
        'birthday',
    ];

/*
    public function classroom(){
        //return $this->belongsTo('App\Models\Classroom', 'classroom_id');
        //return $this->hasOne(Classroom::class);
        return $this->belongsToMany(\App\Models\Classroom::class, 'alumn_classrooms', 'alumn_id','classroom_id');
       /*  return $this->hasOne(Classroom::class)->ofMany([
            'published_at' => 'max',
            'id' => 'max',
        ], function ($query) {
            $query->where('published_at', '<', now());
        }); */
        //return $this->belongsToMany(Classroom::class, 'alumn_classrooms', 'alumn_id','classroom_id');
        /*return $this->hasOne(Classroom::class, AlumnClassroom::class,
    
            'mechanic_id', // Foreign key on the cars table...
            'car_id', // Foreign key on the owners table...
            'id', // Local key on the mechanics table...
            'id' // Local key on the cars table...);        
    } 
*/
    public function classroom(){
        //return $this->classrooms[0];
        return $this->classrooms->first();
    }

    public function subject_absences(){
        return $this->hasMany(\App\Models\SubjectAbsence::class, 'alumn_id', 'id');
    }

    public function docs(){
        return $this->hasMany(\App\Models\AlumnDoc::class, 'alumn_id', 'id');
    }

    public function contacts(){
        return $this->hasMany(\App\Models\AlumnContact::class);
    }
 
    public function alumn_marks(){
        return $this->hasOne(\App\Models\AlumnMark::class, 'alumn_id');
    }

    public function user(){
        return $this->morphOne(\App\Models\User::class, 'userable');
    }

    public function subjects(){
        return $this->belongsToMany(\App\Models\Subject::class, 'alumn_subjects', 'alumn_id','subject_id')
                    ->wherePivot('promotion_id', PromotionScope::getPromotionId());
                     // !important, because if not, when sync (alumnservice) deletes all subjects
                     // laravel bug?? automatically doesn't apply scopes
    }

    public function classrooms(){
        return $this->belongsToMany(\App\Models\Classroom::class, 'alumn_classrooms', 'alumn_id','classroom_id');
    }



    // public function getEmails()
    // {
    //     if (empty($this->attributes['contacts'])) return [];

    //     $result = [];

    //     //\Log::debug($this->contacts);

    //     foreach($this->contacts as $c)
    //     {
    //         if (!empty($c['email']))
    //             $result[] = $c['email'];
    //     }

    //     if (!empty($result))
    //         return $result;
    //     else
    //         return null;
    // }

    public function getCompleteName(){
        return $this->attributes['name'] . ' '.$this->attributes['surname'];
    }

    private function getSubjects(){
        $subjects = self::with('classroom.grade.subjects')->find($this->attributes['id'])->classroom->grade->subjects;
        return $subjects;
    }

    public function getUserDoc($file = '')
    {
        $folder = static::getFolderHash($this->id);
        return 'alumn_documents/'.$folder.'/'.$file;
    }

    static public function getFolderHash($id){
        return md5(config('app.key').$id);
    }

    static public function decryptId($id){
        if (!is_numeric($id))
        {
            return str_replace('alumncole', '', base64_decode($id));
            //return \Crypt::decryptString(base64_decode($id));
        }
        else
            return $id;
    }

    static public function encryptId($id){
        //return base64_encode(\Crypt::encryptString($id));
        return base64_encode('alumncole'.$id);
    }

    public function getPublicId(){
        return static::encryptId((string) $this->id);
    }
}
