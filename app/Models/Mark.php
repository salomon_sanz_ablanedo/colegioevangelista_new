<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mark extends Model
{
    //
    protected $id;
    protected $subject_id;
    protected $classroom_id;
    protected $type_id;
    protected $name;
    protected $description;
    protected $date;

    protected $fillable = [
        'subject_id', 'classroom_id','type_id','name','description','date'
    ];

    protected $casts = [
        'date' => 'datetime:Y-m-d',
    ];

    public function classroom(){
        return $this->belongsTo('App\Models\Classroom', 'classroom_id');
    }

    public function subject(){
        return $this->belongsTo('App\Models\Subject', 'subject_id');
    }

    public function type(){
        return $this->belongsTo(\App\Models\MarkType::class, 'type_id');
    }

    public function alumn_marks($alumn_id = null)
    {
        $result = $this->hasMany(\App\Models\AlumnMark::class, 'mark_id');
        //$result = $this->belongsToMany(\App\Models\AlumnMark::class, 'mark_id',);
        //return $this->belongsToMany('App\Models\Subject', 'alumn_subjects', 'alumn_id','subject_id');


        if (!empty($alumn_id))
            $result->where('alumn_id',$alumn_id);

        return $result;
    }

    public function scopeOfType($query,$value){
        return $query->where('type_id', $value);
    }

    public function scopeOfTypeGlobal($query){
        return $query->whereIn('type_id', [5,6,7,8]);
    }
}
