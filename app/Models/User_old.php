<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User_old extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];

    // protected $hidden = [
    //     'password', 'remember_token',
    // ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userable(){
        return $this->morphTo();
    }

    public function getCurrentCentreId(){
        return $this->centre_id;
    }

    // public function sendPasswordResetNotification($token)
    // {
    //     $this->notify(new \App\Notifications\ResetPasswordNotification($token));
    // }

}
