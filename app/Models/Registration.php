<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\PromotionScope;

class Registration extends Model
{    
    protected 
        $id,
        $public_id,
        $alumn_id,
        $registration_type,
        $grade_id,
        $promotion_id,
        $price,
        $price_subjects,
        $status,
        $send_to;
    
    protected $fillable = [
        'public_id',
        'alumn_id',
        'registration_type',
        'grade_id',
        'promotion_id',
        'status',
        'price',
        'price_subjects',
        'send_to'
    ];

    public $timestamps = true;

    public function alumn(){
        return $this->belongsTo('App\Models\Alumn', 'alumn_id');
    }

    public function grade(){
        return $this->belongsTo('App\Models\Grade', 'grade_id');
    }

    public function promotion(){
        return $this->belongsTo('App\Models\Promotion', 'promotion_id');
    }

    public function generatePublicId(){
        return md5($this->alumn_id);//.'|'.$this->grade_id.'|'.$this->registration_type.'|'.$this->promotion_id);
    }

    protected static function booted(){
        static::addGlobalScope(new PromotionScope);
    }    
}
