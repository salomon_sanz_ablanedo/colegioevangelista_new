<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\CentreScope;

class Centre extends Model
{
    protected 
        $id,
        $name;        

    public $timestamps = true;        
}
