<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\CentreScope;

class Teacher extends Model
{
    use CentreScope;

    protected 
        $id,
        $name,
        $surname,
        $centre_id,
        $photo,
        $tutor;

    public $timestamps = true;

    public function subjects(){
        return $this->belongsToMany(\App\Models\Subject::class, 'subject_teachers', 'teacher_id', 'subject_id');
    }
 
    /* public function tutor_class(){
        return $this->belongsTo(\App\Models\Classroom::class, 'id','tutor_teacher_id');
    }  */


    // to enable one teacher can be tutor in many classess
    public function tutor_class(){
        return $this->hasMany(\App\Models\Classroom::class, 'tutor_teacher_id');
    }

    public function user(){
         return $this->morphOne('App\Models\User', 'userable');
    }
}
