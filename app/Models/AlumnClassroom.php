<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\AlumnSubject;
use App\Scopes\PromotionScope;
use Exception;

class AlumnClassroom extends Model
{        
    protected $id;
    protected $alumn_id;
    protected $classroom_id;
    protected $promotion_id;

    protected $fillable = [
        'alumn_id','classroom_id','promotion_id'
    ];

    protected static function booted(){
        static::addGlobalScope(new PromotionScope);
    }    
}
