<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\CentreScope;

class Promotion extends Model
{
    use CentreScope;

    protected 
        $id,
        $name,
        $centre_id,
        $prev_promotion,
        $next_promotion,
        $date_start,
        $date_finish,
        $active;

    protected $casts = [
        'date_start' => 'date:Y-m-d',
        'date_finish' => 'date:Y-m-d',
    ];

    public $timestamps = false;  
    
    public function nextPromotion(){
        return $this->belongsTo('App\Models\Promotion', 'next_promotion');
    }

    public function prevPromotion(){
        return $this->belongsTo('App\Models\Promotion', 'prev_promotion');
    }

    protected function serializeDate($date): string
    {
        return $date->format('d-m-Y');
    }
}
