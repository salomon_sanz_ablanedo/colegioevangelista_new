<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GradeClassTeacher extends Model
{
    //
    protected $table = 'grades_classes_teachers';

    public function subject(){
        return $this->hasOne('App\Models\GradeSubject', 'id', 'grade_subject_id');
    }

    public function classroom(){
        return $this->hasOne('App\Models\Classroom', 'id', 'classroom_id');
    }
}
