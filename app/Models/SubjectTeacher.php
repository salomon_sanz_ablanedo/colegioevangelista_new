<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubjectTeacher extends Model
{
    protected
        $id,
        $subject_id,
        $teacher_id;

    public function subject(){
        return $this->hasOne('App\Models\Subject', 'id','subject_id');
    }

    public function teacher(){
        return $this->hasOne('App\Models\Teacher', 'id','teacher_id');
    }
}
