<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $id;
    protected $subject;
    protected $html_text;
    protected $recipients_groups;
    protected $other_emails;
    protected $computed_recipients;
    protected $total_recipients;
    protected $status;

    protected $casts = [
        'recipients_groups' => 'array'
    ];

     public function attachments(){
        return $this->hasMany('App\Models\NewsletterAttachment', 'newsletter_id', 'id');
    }

    // const STATUS_PENDING = 'PENDING';
    // const STATUS_SENT = 'SENT';

    // protected $fillable = [
    //     'id',
    //     'subject',
    //     'html_text',
    //     'recipients_emails',
    //     'recipients_groups',
    //     'status'
    // ];
}
