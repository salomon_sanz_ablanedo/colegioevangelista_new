<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassroomLog extends Model
{   
    public function grade(){
        return $this->hasOne('App\Models\Grade','id','grade_id');
    }

    public function classroom(){
        return $this->belongsTo(\App\Models\Classroom::class, 'classroom_id');
    }

    public function teacher(){
        return $this->belongsTo(\App\Models\Teacher::class, 'teacher_id');
    }

    public function subject(){
        return $this->belongsTo(\App\Models\Subject::class, 'subject_id');
    }
}