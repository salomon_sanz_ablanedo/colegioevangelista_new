<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    //
    protected $id;
    protected $number;
    protected $cycle_id;

    protected $appends = ['complete_name'];

    public function cycle(){
        return $this->belongsTo('App\Models\Cycle', 'cycle_id');
    }

    public function subjects(){
         return $this->hasMany('App\Models\Subject', 'grade_id');
    }

    public function getCompleteNameAttribute(){
        return $this->attributes['number'] . 'º ' . $this->cycle['short_name'];
    }

    public function getCompleteNameLongAttribute(){
        return $this->attributes['number'] . 'º ' . $this->cycle['name'];
    }
}
