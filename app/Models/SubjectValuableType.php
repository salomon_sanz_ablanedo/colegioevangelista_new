<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubjectValuableType extends Model
{
    protected
        $id,
        $name,
        $subject_id,
        $percent;

    public $timestamps = true;

    public function subject(){
        return $this->belongsTo('App\Models\Subject', 'subject_id');
    }
}