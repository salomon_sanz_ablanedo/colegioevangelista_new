<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\PromotionScope;

class SubjectAbsence extends Model
{    
    protected
        $id,
        $date,
        $subject_id,
        $alumn_id,
        $justified,
        $type;

    public $timestamps = true;

    protected $dates = ['date'];

    public function subject(){
        return $this->belongsTo('App\Models\Subject', 'subject_id');
    }

    public function alumn(){
        return $this->belongsTo('App\Models\Alumn', 'alumn_id');
    }

    protected static function booted(){
        static::addGlobalScope(new PromotionScope);
    }
}