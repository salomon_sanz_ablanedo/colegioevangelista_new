<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\AlumnSubject;

use Exception;

class AlumnMarkArea extends Model
{
    protected $id;
    protected $alumn_mark_id;
    protected $subject_area_id;
    protected $value;

    public $timestamps = false;

    public function alumn_mark(){
        return $this->belongsTo('App\Models\AlumnMark','alumn_mark_id');
    }

    public function area(){
        return $this->belongsTo('App\Models\SubjectArea', 'subject_area_id');
    }
}
