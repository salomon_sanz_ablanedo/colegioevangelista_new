<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID','AKIAJG2G77XNIYXODCQQ'),
        'secret' => env('AWS_SECRET_ACCESS_KEY','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'),
        'region' => env('AWS_DEFAULT_REGION', 'eu-west-1'),
    ],

    'google' => [
        'client_id' => '713189246141-gt7e7sjirf1m7hg12e22169qi1lto2do.apps.googleusercontent.com',//env('GOOGLE_CLIENT_ID'),
        'client_secret' => 'aOiCGdlxwxaUgsYm88EO1dII', //env('GOOGLE_CLIENT_SECRET'),
        'redirect' => '/admin/google/callback',
		'hd' => 'colegioevangelista.com'//env('GOOGLE_CLIENT_HD', '*')
    ],

];
