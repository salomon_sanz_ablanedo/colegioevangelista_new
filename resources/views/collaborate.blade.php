@extends('layouts.simple')

@push('css')
<style>
    body{
        background: linear-gradient(to bottom, rgba(92, 77, 66, 0.8) 0%, rgba(92, 77, 66, 0.8) 100%), url(/images/bg-masthead.jpg?4faf4a7…);
        background-position: center;
        background-repeat: repeat;
        background-attachment: scroll;
        background-size: cover;
        font-family:"Merriweather Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
    }
p {
    font-size:12px;
}
.table_bank td{
    border-top:0px !important;
}
</style>
@endpush

@section('content')
    <div class="row align-items-center justify-content-center text-center">
        <div style="width:100%; background:linear-gradient(180deg, rgba(34,193,195,1) 0%, rgb(235 200 124) 37%, rgba(255,255,255,1) 65%);">
            <div class="col-lg-12 align-self-start pt-5">
                {{-- <img src="{{ asset('img/logo_escudo.png') }}" width="150" class="mb-3"/> --}}
                    <h1 class="text-uppercase font-weight-bold">Colabora con nosotros</h1>
                    <hr class="divider my-4" />
            </div>        
            <div class="col-lg-12 align-self-baseline">
                <p class="font-weight-light">Ayúdanos a construir un nuevo modelo educativo</p>
                {{-- <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a> --}}            
            </div>
            <div class="col-12">
                <iframe style="max-width:560px;" width="100%" height="315" src="https://www.youtube.com/embed/NU3Hxb000bc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>    
            
            <div class="py-2 px-5 text-left pt-5">

                El Colegio e Instituto Evangelista nació en 2013, en base a dos <b>necesidades</b>:  <br/><br/>

                <div class="">               
                    <ul>
                        <li>1a. - Que nuestros hijos recibiesen una educación coherente con los principios recibidos en el hogar: autoridad, respeto, honestidad, verdad, valor, integridad y sentido del honor</li>
                        <li>2a. - Que fuera un colegio accesible para todas aquellas familias que quisieran este tipo de educación, independientemente de su condición social o económica </li>
                    </ul>                 
                </div>

                Actualmente impartimos primaria y secundaria y tenemos en proyecto el próximo curso bachillerato. Nuestros alumnos en este momento tienen un alto nivel académico y una formación en valores importante para sus relaciones y para su futuro.

                <br/><br/>
                El objetivo de que sea un colegio privado accesible para todas las familias que quieran este tipo de educación supone que los ingresos del Colegio, mediante las cuotas de familias, no son suficientes para sustentar el funcionamiento habitual del Colegio. Para ello creamos, lo que hoy conocemos como “amigos del colegio”, que son personas e instituciones que adquieren con el centro un compromiso económico, mensual trimestral o anual.
                Estas aportaciones más las cuotas de los alumnos permiten que muchas familias, hoy 59 matriculen a sus hijos en el colegio e instituto y reciban esta formación que muchos niños necesitan. 
                <br/><br/>
                <b>Con esta campaña queremos pedir tu colaboración y tu compromiso para ayudarnos económicamente y poder así continuar con esta labor.</b>
                {{-- <div class="p-3" style="background-color:bisque;">
                    
                </div> --}}
                <br/><br/>
                Las aportaciones, son <u>desgrabables</u>, dado que está canalizada a través la ASOCIACIÓN CRISTIANA VIDA NUEVA, que tiene un proyecto específico para ayudar a la educación de los niños, y al ser una asociación de utilidad pública ofrece desgravaciones para las personas físicas (IRPF) y para las personas jurídicas (Impuesto de Sociedades).
            </div>
        </div>
    </div>
    <div class="row px-4 text-muted small text-left">
        <section class="page-section pb-0 pt-5 col-12">
            <div class="text-center">
                <h5 class="mt-0"> 
                    ¿Cómo puedo donar?
                </h5>
                <hr class="divider"/>
            </div>
            <div class="row justify-content-center">
                <div class="col-sm-6 text-center my-4">
                    <h6>Pago Online</h6>
                    <div class="p-3 text-center" style="background-color:#d2eaff; border-radius:12px; min-height:135px;">
                        <br/>
                        <a class="btn btn-primary" href="https://www.centrovidanueva.es/colaboracolegio">Realizar una donación online</a><br/><br/>
                        <small class="text-muted">Serás redirigido a la página del Centro Vida Nueva</small>
                    </div>                                        
                </div>
                <div class="col-sm-6 text-center my-4">
                    <h6>Transferencia bancaria</h6>
                    <div class="p-3 text-left" style="background-color:#d2ffd3; border-radius:12px; min-height:135px;">
                        <div class="row mb-2 mt-3">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">IBAN</label>
                            <div class="col-sm-10 input-group">
                                <span id="input_iban" style="font-family:Arial; font-size:1.2rem; background-color: #FFFF; padding: 5px 12px;" onclick="selectText(event)">ES50 2100 5174 89 2100137465</span>
                                {{-- <input type="text" class="form-control" id="input_iban" style="background-color:#FFFFFF; font-family:Arial" readonly value="ES50 2100 5174 89 2100137465" onClick="copyIban()"> --}}
                                {{-- <button class="btn btn-outline-secondary" type="button" id="button-addon2" style="background-color:#FFF;" alt="copiar" title="copiar" onclick="copyIban()"><i class="fas fa-clone"></i></button> --}}
                            </div>
                        </div>
                        <div class="row mt-2">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Concepto</label>
                            <div class="col-sm-10">                                            
                                {{-- <input type="text" class="form-control" id="input_iban" style="background-color:#FFFFFF; font-family:Arial" readonly value="&quot;Proyecto educativo&quot; nombre y apellidos" onclick="selectText(event)"> --}}
                                <span id="input_iban" style="font-family:Arial; font-size:1rem; padding: 12px 14px; border-radius: 10px; line-height: 32px;" onclick="selectText(event)">&quot;Proyecto educativo&quot; nombre y apellidos</span>
                            </div>
                        </div>
                        {{--  <table class="table table_bank" style="margin-bottom: 0px !important; font-family:Arial">
                            <tbody>
                                <tr>
                                    <td scope="row">IBAN</td>
                                    <td><b><input type="text" value="ES50 2100 5174 89 2100137465" class="form-control" readonly/></b></td>
                                </tr>
                                <tr>
                                    <td scope="row">Concepto</td>
                                    <td>
                                    <b><i>&quot;Proyecto educativo&quot; nombre y apellidos</i></b>
                                    </td>
                                </tr>
                            </tbody>
                        </table>    --}}
                    </div>
                </div>
            </div>            
        </section>                    
    </div>

    <section class="page-section pb-0 px-5 px-md-2">
        <div class="">
            <div class="text-center">
                <h5 class=" mt-0"> 
                    ¿Cuánto me voy a desgravar?
                </h5>
                <hr class="divider my-4" />
            </div>
            <div class="row justify-content-center">
                <div class="col-12">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th scope="row">Domicilio Fiscal en Navarra</th>
                                <td>25% de las donaciones</td>
                            </tr>
                            <tr>
                                <th scope="row">Domicilio en otra parte del estado</th>
                                <td>
                                    80% de los primeros 150 euros
                                    <br>35% a partir de los 150 euros<br/>
                                    <span class="small text-muted">
                                    Ejemplo: si donas 20 euros al mes (240 euros al año), podrás deducirte 151,50&euro; en tu declaración de la renta anual. 
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Persona física en el Pais Vasco</th>
                                <td>30%</td>
                            </tr>
                        </tbody>
                    </table>                                        
                </div>
            </div>
            <div class="row">
                <div class="col-12 p-4 text-muted small">
                    Una vez realizada la donación, puedes pedir tu certificado poniendo en contacto con nosotros: <a href="mailto:veronica@centrovidanueva.org.es">veronica@centrovidanueva.org.es</a>
                </div>
            </div>                        
        </div>
    </section>
@endsection
        
<!-- Footer-->
@push('js')
<script>
    function copyIban(){
        var copyText = document.getElementById("input_iban");  
        copyText.select();
        copyText.setSelectionRange(0, 99999); /* For mobile devices */   
        navigator.clipboard.writeText(copyText.value);  
        document.getElementById('iban_msg').innerText = 'IBAN copiado'
    }

    function selectText(e)
    {
        var range, selection;
        debugger;
        if (window.getSelection && document.createRange) {
            selection = window.getSelection();
            range = document.createRange();
            range.selectNodeContents(e.target);
            selection.removeAllRanges();
            selection.addRange(range);
        } else if (document.selection && document.body.createTextRange) {
            range = document.body.createTextRange();
            range.moveToElementText(e.target);
            range.select();
        }
    }
</script>
@endpush