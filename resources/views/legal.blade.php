@extends('layouts.simple')

@section('content')
    <div class="row align-items-center justify-content-center text-center pt-5">
        <div class="col-lg-12 align-self-start ">
        {{-- <img src="{{ asset('img/logo_escudo.png') }}" width="150" class="mb-3"/> --}}
            <h1 class="text-uppercase font-weight-bold">Aviso legal</h1>
            <hr class="divider my-4" />
        </div>
        {{-- <div class="col-lg-12 align-self-baseline">
            <p class="font-weight-light">información</p>
            <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>            
        </div> --}}
        <div class="py-2 px-5 text-muted small text-left">

            <h5 class="mt-0">Titular del presente portal</h5>

            <div class="p-3 m-2" style="border-radius:12px; border:1px solid #CCC;">                        
            Asociación Cristiana de Recursos Educativos Nueva Generación<br/>
            NIF G71173041 <br/>
            Domicilio: Calle Virgen del Puy, 5 bajo<br>
            Código Postal: 31011. <br/>
            Pamplona (Navarra)
            </div>
            </p>
<h5>Propiedad intelectual e industrial</h5>
El diseño del portal y sus códigos fuente, así como los logos, marcas y demás signos distintivos que aparecen en el mismo pertenecen a Asociación Cristiana de Recursos Educativos Nueva Generación y están protegidos por los correspondientes derechos de propiedad intelectual e industrial.
<h5>Responsabilidad de los contenidos</h5>
<p>Asociación Cristiana de Recursos Educativos Nueva Generación no se hace responsable de la legalidad de otros sitios web de terceros desde los que pueda accederse al portal. Tampoco responde por la legalidad de otros sitios web de terceros, que pudieran estar vinculados o enlazados desde este portal.</p>
<p>Asociación Cristiana de Recursos Educativos Nueva Generación se reserva el derecho a realizar cambios en el sitio web sin previo aviso, al objeto de mantener actualizada su información, añadiendo, modificando, corrigiendo o eliminando los contenidos publicados o el diseño del portal.</p>
<p>Asociación Cristiana de Recursos Educativos Nueva Generación no será responsable del uso que terceros hagan de la información publicada en el portal, ni tampoco de los daños sufridos o pérdidas económicas que, de forma directa o indirecta, produzcan o puedan producir perjuicios económicos, materiales o sobre datos, provocados por el uso de dicha información.</p>
<h5>Ley aplicable</h5>
<p>La ley aplicable en caso de disputa o conflicto de interpretación de los términos que conforman este aviso legal, así como cualquier cuestión relacionada con los servicios del presente portal, será la ley española.</p>

        </div>
    </div>  
@endsection