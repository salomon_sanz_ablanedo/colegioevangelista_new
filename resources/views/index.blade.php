@extends('layouts.master')

@section('main')        
        <!-- Masthead-->
        <header class="masthead">
            <div class="container h-100">
                <div class="row h-100 align-items-center justify-content-center text-center">

                    <div class="col-lg-10 align-self-end">
                    <img src="{{ asset('img/logo_escudo.png') }}" width="150" class="mb-3"/>
                        <h1 class="text-uppercase text-white font-weight-bold">Colegio Instituto <br>Evangelista</h1>
                        <hr class="divider my-4" />
                    </div>
                    <div class="col-lg-8 align-self-baseline">
                        <p class="text-white-75 font-weight-light mb-5">En busca de una generación digna de confianza</p>
                        {{-- <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a> --}}
                    </div>
                </div>
            </div>
        </header>

        <section class="page-section pb-4" id="matriculas" style="background-color: #cbcccc40;">
            <div class="container">

                <div class="row justify-content-center mt-3 mb-4">
                    <div class="col-lg-8 text-center">
                        <h4 class="mt-0">Calendario 2024-2025</h4>
                        <hr class="divider my-4"/>
                        <p class="">
                            Ya puedes descargarte a continuación el calendario del próximo curso contacto (actualizado el 27/08/24). <br/><br/><a href="/documents/CALENDARIO_2024-2025.xls" target="_blank"> Descargar calendario</a>
                        </p>
                    </div>                    
                </div>  

                {{-- <div class="row justify-content-center mt-3 mb-4">
                    <div class="col-lg-8 text-center">
                        <h4 class="mt-0">Resultados del sorteo</h4>
                        <hr class="divider my-4"/>
                        <p class="">
                        1er premio: <b>384</b> - Recogido <br>
                        2o premio: <b>1255</b> -  No reclamado. Donado a Vida Nueva<br>
                        3er premio: <b>1673</b> - Recogido
                        </p>
                    </div>                    
                </div>    

               <div class="row justify-content-center mt-3 mb-4">
                    <div class="col-lg-8 text-center">
                        <h4 class="mt-0">Bases del sorteo cestas de Navidad</h4>
                        <hr class="divider my-4"/>

                        <p class="">
                            A continuación facilitamos las bases del sorteo que se realizará el próximo 23 de diciembre. <br/><br/><a href="https://docs.google.com/document/d/15VUR5kHPHbuT6HHsykQ26LGUGFoUZzTj/edit?usp=sharing&ouid=109721736292847791834&rtpof=true&sd=true" target="_blank"> Bases del sorteo</a>
                        </p>
                    </div>                    
                </div>       --}}


                {{-- <div class="row justify-content-center">
                    <div class="col-lg-12 text-center">                    
                        <table class="table">
                            <tr>
                                <td class="text-right"><b>E.S.O</b></td>
                                <td class="text-left">Alumnos nuevos o cambio de primaria a secundaria (6º).</td>
                            </tr>
                            <tr>
                                <td class="text-right"><b>1º BACHILLER</b></td>
                                <td class="text-left">Para alumnos nuevos o cambio de Secundaria a bachiller (4º).</td>
                            </tr>
                        </table>
                    </div>                    
                </div>
                <br/>
                        <hr class="divider my-4" />

               {{--  <div class="row justify-content-center mt-4">
                    <div class="col-lg-8 text-center">
                        <h4 class="mt-0">Periodo de matriculación extraordinaria</h4>
                    </div>
                </div>
                <div class="row justify-content-center mb-4 mt-3">
                    <div class="col-lg-8 text-center">
                        <p class="text-muted small">
                            El periodo de matriculación extraordinaria serán los días 1 y 2 de septiembre, de 9:30 a 13:30.
                        </p>
                    </div>
                </div>
                
                <hr class="divider my-4"> --}}


              {{--  <div class="row justify-content-center mt-4">
                    <div class="col-lg-8 text-center">
                        <h4 class="mt-0">Calendario 2022-2023</h4>
                <hr class="divider my-4">

                    </div>
                    
                </div>
                <div class="row justify-content-center mb-4 mt-3">
                    <div class="col-lg-8 text-center">
                        <p class="">
                            Ya puedes descargarte a continuación el calendario del próximo curso contacto (actualizado el 16/07/22). <br/><br/><a href="/documents/CALENDARIO_2022-2023.xls" target="_blank"> Descargar calendario</a>
                        </p>
                    </div>
                </div> --}}
                

               {{--  <div class="row justify-content-center mt-4">
                    <div class="col-lg-8 text-center">
                        <h4 class="mt-0">Mascarillas</h4>
                    </div>
                </div>

                <div class="row justify-content-center mb-4 mt-3">
                    <div class="col-lg-8 text-center">
                        <p class="text-muted small">
                            A continuación, facilitamos los enlaces para adquirir las mascarillas del colegio. No olvides escribir el nombre y el curso:
                            <br/><br/>
                            
                                <a href="https://wanapix.es/design/mascarilla-primaria/L7wl7l5xs8qi?select_article=15562" target="_blank">Modelo para Ludoteca</a> | 
                                <a href="https://wanapix.es/design/mascarilla-primaria/08u4z27wxrmd?select_article=15562" target="_blank">Modelo para Primaria</a> | 
                                <a href="https://wanapix.es/design/mascarilla-primaria/2cap084g97ek?select_article=15562" target="_blank">Modelo para Secundaria</a>
                            
                            <br/><br/>
                            Además, se puede utilizar el código de promoción: <b>COLEGIOEVANGELISTA</b> para obtener un precio de descuento especial.                        
                        </p>
                    </div>
                </div> --}} 

                {{-- <div class="row justify-content-center mt-4">
                    <div class="col-lg-8 text-center">
                        <h4 class="mt-0">Libros y Material Curso 2022-2023</h4>
                    </div>
                </div>
                <div class="row justify-content-center mb-4 mt-3">
                    <div class="col-lg-8 text-center">
                        <p class="text-muted small">
                            Para realizar la compra de los libros a través del colegio, será necesario descargarse el documento con el listado de libros del respectivo curso (a continuación) y enviarlo rellenado por email a la dirección de <a href="#contact" class="js-scroll-trigger">contacto.</a>
                            <br/>(actualizado el 26/07/22).
                        </p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <table class="table small"> --}}
                            {{-- <tr>
                                <td>Ludoteca 1</td>
                                <td>
                                    <a href="/documents/LUDOTECA_1_LIBROS.docx" target="_blank">Libros</a> | 
                                    <a href="/documents/LUDOTECA_MATERIALES.docx" target="_blank">Materiales</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Ludoteca 2</td>
                                <td>
                                    <a href="/documents/LUDOTECA_2_LIBROS.docx" target="_blank">Libros</a> | 
                                    <a href="/documents/LUDOTECA_MATERIALES.docx" target="_blank">Materiales</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Ludoteca 3</td>
                                <td>
                                    <a href="/documents/LUDOTECA_3_LIBROS.docx" target="_blank">Libros</a> | 
                                    <a href="/documents/LUDOTECA_MATERIALES.docx" target="_blank">Materiales</a>
                                </td>
                            </tr>
                            <tr>
                                <td>1º Educación Primaria</td>
                                <td>
                                    <a href="/documents/PRIMARIA_1_LIBROS.docx" target="_blank">Libros</a> | 
                                    <a href="/documents/PRIMARIA_MATERIALES.docx" target="_blank">Materiales</a>
                                </td>
                            </tr>
                            <tr>
                                <td>2º Educación Primaria</td>
                                <td>
                                    <a href="/documents/PRIMARIA_2_LIBROS.docx" target="_blank">Libros</a> | 
                                    <a href="/documents/PRIMARIA_MATERIALES.docx" target="_blank">Materiales</a>
                                </td>
                            </tr>
                            <tr>
                                <td>3º Educación Primaria</td>
                                <td>
                                    <a href="/documents/PRIMARIA_3_LIBROS.docx" target="_blank">Libros</a> | 
                                    <a href="/documents/PRIMARIA_MATERIALES.docx" target="_blank">Materiales</a>
                                </td>
                            </tr>
                            <tr>
                                <td>4º Educación Primaria</td>
                                <td>
                                    <a href="/documents/PRIMARIA_4_LIBROS.docx" target="_blank">Libros</a> | 
                                    <a href="/documents/PRIMARIA_MATERIALES.docx" target="_blank">Materiales</a>
                                </td>
                            </tr>
                            <tr>
                                <td>5º Educación Primaria</td>
                                <td>
                                    <a href="/documents/PRIMARIA_5_LIBROS.docx" target="_blank">Libros</a> | 
                                    <a href="/documents/PRIMARIA_MATERIALES.docx" target="_blank">Materiales</a>
                                </td>
                            </tr>
                            <tr>
                                <td>6º Educación Primaria</td>
                                <td>
                                    <a href="/documents/PRIMARIA_6_LIBROS.docx" target="_blank">Libros</a> | 
                                    <a href="/documents/PRIMARIA_MATERIALES.docx" target="_blank">Materiales</a>
                                </td>
                            </tr>
                            <tr>
                                <td>1º E.S.O</td>
                                <td>
                                    <a href="/documents/SECUNDARIA_1_LIBROS.docx" target="_blank">Libros</a>
                                </td>
                            </tr>
                            <tr>
                                <td>2º E.S.O</td>
                                <td>
                                    <a href="/documents/SECUNDARIA_2_LIBROS.docx" target="_blank">Libros</a>
                                </td>
                            </tr>
                            <tr>
                                <td>3º E.S.O</td>
                                <td>
                                    <a href="/documents/SECUNDARIA_3_LIBROS.docx" target="_blank">Libros</a>
                                </td>
                            </tr>
                            <tr>
                                <td>4º E.S.O</td>
                                <td>
                                    <a href="/documents/SECUNDARIA_4_LIBROS.docx" target="_blank">Libros</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </section> --}}
        <!-- About-->
        <section class="page-section pb-0" id="vision">
            <div class="container">
                <div class="row justify-content-center mb-4">
                    <div class="col-lg-8 text-center">
                        <h2 class="mt-0">Visión</h2>
                        <hr class="divider my-4">
                        <p class="mb-4">Dios, como Creador de la humanidad, nos dejó el legado del libro de los libros, la Biblia, la cual es el mensaje del Padre de Amor donde nos da visión y nos enseña el camino de la verdadera sabiduría.</p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-5 text-center">
                        <pre class="mb-4 mr-1" style="font-style: italic">
"Adquiere sabiduría, adquiere inteligencia
Sabiduría, ante todo; adquiere sabiduría;
Y sobre todas tus posesiones adquiere  inteligencia.
Engrandécela, y ella te engrandecerá;
Ella te honrará cuando tú la hayas abrazado"

                            Proverbios 4
                        </pre>
                    </div>
                    <div class="col-lg-5 text-center ml-1" style="border-left: 1px solid #CCC">
<pre class="mb-4" style="font-style: italic">
"¿Dónde se halla la sabiduría?
¿Dónde está el lugar de la inteligencia?
No conoce su valor el hombre.
La sabiduría es mejor que las piedras preciosas,
Dios conoce su lugar y dijo al hombre;
He aquí que el temor del Señor es la sabiduría
Y el apartarse del mal, la inteligencia"
                        
                                Job 28
                        </pre>
                    </div>
                </div>
            </div>
        </section>
       {{--  <section class="page-section pb-0" id="colaborate" style="background-color:#0f4e89; color:white;">
            <div class="container">
                <div class="text-center">
                    <h2 class=" mt-0"> 
                        Colabora con nosotros
                    </h2>
                    <hr class="divider my-4" />
                </div>
                <div class="row justify-content-center pb-4">
                    <div class="col-5"> --}}
                        {{-- <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" href="#">Hazte amigo del colegio</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Hacer una donación puntual</a>
                            </li>
                        </ul>     --}}                                            
                        {{-- <div class="p-4 text-center" style="background-color:white; color:black; border-radius:12px;">
                            ¿Cuanto quieres donar?<br/><br/>
                            <button type="button" class="btn btn-lg btn-primary donate_quantity_opt">10&euro;</button>
                            <button type="button" class="btn btn-lg btn-primary donate_quantity_opt">20&euro;</button>
                            <button type="button" class="btn btn-lg btn-primary donate_quantity_opt">30&euro;</button>                            
                            <button type="button" class="btn btn-lg btn-primary donate_quantity_opt">
                                <input type="number" min="5" step="5" class="text-center" style="width:65px;" placeholder="XX"/>&euro;
                            </button>
                            <br/>
                            <br/>
                            <input type="radio" name="donation_periodity"/> Mensual &nbsp;&nbsp;&nbsp; <input type="radio" name="donation_periodity"/> Puntual
                            <br/>
                            <br/>
                            <button class="btn btn-primary">Continuar »</button>
                        </div>
                    </div>
                    <div class="col-7 p-4" style="border-radius:12px; background-color:lightblue; color:black;">
                        Al ser un centro privado, el Colegio Evangelista se mantiene en gran parte gracias a las ayudas y donaciones particulares.
                        <br/><br/>
                        Tu aportación es vital para seguir ayudándonos a mantener el proyecto vivo y seguir llegando a más familias y alumnos.<br/><br/>     
                        Además, tu ayuda posee incentivos fiscales gracias al Mecenazgo Social. <br><small class="text-muted text-italic">Las personas físicas se podrán deducir fiscalmente en su declaración del IRPF el 80% para los 150 primeros euros donados a entidades sociales y del 35% a partir de esa cantidad.</small>
                    </div>
                </div>
            </div>
        </section> --}}
        <!-- About-->
        <section class="page-section pb-0" id="ubication">
            <div class="">
                <div class="text-center">
                    <h2 class=" mt-0"> 
                         Ubicación
                    </h2>
                    <hr class="divider my-4" />
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2926.232835038343!2d-1.68962488485036!3d42.82569527915834!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd508d6f164e10d3%3A0x856ea60807a2a8dd!2sColegio%20Evangelista%20Primaria!5e0!3m2!1ses!2ses!4v1587673814893!5m2!1ses!2ses" height="450" frameborder="0" style="border:0; width:100%;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>

                    {{-- <div class="col-lg-8 text-center">
                        <h2 class="text-white mt-0">We've got what you need!</h2>
                        <hr class="divider light my-4" />
                        <p class="text-white-50 mb-4">Start Bootstrap has everything you need to get your new website up and running in no time! Choose one of our open source, free to download, and easy to use themes! No strings attached!</p>
                        <a class="btn btn-light btn-xl js-scroll-trigger" href="#services">Get Started!</a>
                    </div> --}}
                </div>
            </div>
        </section>
        <!-- Services-->
        {{-- <section class="page-section" id="services">
            <div class="container">
                <h2 class="text-center mt-0">At Your Service</h2>
                <hr class="divider my-4" />
                <div class="row">
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="mt-5">
                            <i class="fas fa-4x fa-gem text-primary mb-4"></i>
                            <h3 class="h4 mb-2">Centro Privado</h3>
                            <p class="text-muted mb-0">Educación Infantil, Ed. Primaria y Ed. Secundaria</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="mt-5">
                            <i class="fas fa-4x fa-laptop-code text-primary mb-4"></i>
                            <h3 class="h4 mb-2">Up to Date</h3>
                            <p class="text-muted mb-0">All dependencies are kept current to keep things fresh.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="mt-5">
                            <i class="fas fa-4x fa-globe text-primary mb-4"></i>
                            <h3 class="h4 mb-2">Ready to Publish</h3>
                            <p class="text-muted mb-0">You can use this design as is, or you can make changes!</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 text-center">
                        <div class="mt-5">
                            <i class="fas fa-4x fa-heart text-primary mb-4"></i>
                            <h3 class="h4 mb-2">Made with Love</h3>
                            <p class="text-muted mb-0">Is it really open source if it's not made with love?</p>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}
        <!-- Portfolio-->
        {{-- <section id="portfolio">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/1.jpg"
                            ><img class="img-fluid" src="assets/img/portfolio/thumbnails/1.jpg" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div></a
                        >
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/2.jpg"
                            ><img class="img-fluid" src="assets/img/portfolio/thumbnails/2.jpg" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div></a
                        >
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/3.jpg"
                            ><img class="img-fluid" src="assets/img/portfolio/thumbnails/3.jpg" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div></a
                        >
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/4.jpg"
                            ><img class="img-fluid" src="assets/img/portfolio/thumbnails/4.jpg" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div></a
                        >
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/5.jpg"
                            ><img class="img-fluid" src="assets/img/portfolio/thumbnails/5.jpg" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div></a
                        >
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/6.jpg"
                            ><img class="img-fluid" src="assets/img/portfolio/thumbnails/6.jpg" alt="" />
                            <div class="portfolio-box-caption p-3">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div></a
                        >
                    </div>
                </div>
            </div>
        </section> --}}
        <!-- Call to action-->
        {{-- <section class="page-section bg-dark text-white">
            <div class="container text-center">

                <h2 class="mb-4">
                        <i class="fas fa-map-marker text-muted"></i>
                    
                    Ubicación</h2>
                <div class="row">
                    <div class="col-6">
                        <a class="btn btn-light btn-xl" href="https://startbootstrap.com/themes/creative/">Download Now!</a>
                    </div>
                    <div class="col-6">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2926.232835038343!2d-1.68962488485036!3d42.82569527915834!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd508d6f164e10d3%3A0x856ea60807a2a8dd!2sColegio%20Evangelista%20Primaria!5e0!3m2!1ses!2ses!4v1587673814893!5m2!1ses!2ses" height="450" frameborder="0" style="border:0; width:100%;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
            </div>
        </section> --}}
        <!-- Contact-->
        <section class="page-section mb-5" id="contact">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <h2 class="mt-0">Contacta con nosotros</h2>
                        <hr class="divider my-4" />
                        <p class="text-muted mb-5">Si quieres más información no dudes en contactar con nosotros </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 ml-auto text-center mb-5 mb-lg-0">
                        <i class="fas fa-phone fa-3x mb-3 text-muted"></i>
                        <div class="small">
                            +34 948 343 926                            
                        </div>
                    </div>
                    <div class="col-lg-4 mr-auto text-center">
                        <i class="fas fa-envelope fa-3x mb-3 text-muted"></i><!-- Make sure to change the email address in BOTH the anchor text and the link target below!-->
                        <a class="d-block" href="mailto:administracion@colegioevangelista.com">administracion@colegioevangelista.com</a>
                    </div>
                </div>
            </div>
        </section>            

     

            
@endsection
