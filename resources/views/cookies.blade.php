@extends('layouts.simple')

@section('content')
    <div class="row align-items-center justify-content-center text-center pt-5">
        <div class="col-lg-12 align-self-start ">
        {{-- <img src="{{ asset('img/logo_escudo.png') }}" width="150" class="mb-3"/> --}}
            <h1 class="text-uppercase font-weight-bold">Política de Cookies</h1>
            <hr class="divider my-4" />
        </div>
        {{-- <div class="col-lg-12 align-self-baseline">
            <p class="font-weight-light">información</p>
            <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>            
        </div> --}}
        <div class="py-2 px-5 text-muted small text-left">

            <h5 class="mt-0">¿Qué son las Cookies</h5>

           <p>
           Las cookies son archivos de texto que contienen pequeñas cantidades de información y que se descargan en su dispositivo cuando visitas una página web. Las cookies permiten a una página web reconocer su dispositivo y sus preferencias, proveyendo información al servidor de la página web con la que puede mejor su experiencia de navegación.
            </p>
            
            <h5 class="mt-0">Cookies usadas en este sitio web</h5>
            <p>
            Le informamos, en cumplimiento de la normativa vigente, que este sitio web utiliza cookies propias y de terceros. Las cookies utilizadas son en concreto las siguientes:
            </p>

            <table class="table">
                <tbody>
                    <tr>
                        <th>Nombre</th>
                        <th>Proveedor</th>
                        <th>Finalidad</th>
                        <th>Duración</th>
                    </tr>
                    <tr>
                        <td>1p_JAR</td>                        
                        <td>Google</td>                        
                        <td>Transfiere información de navegación a Google</td>                        
                        <td>1 semana</td>                        
                    </tr>   
                    <tr>
                        <td>CONSENT</td>                        
                        <td>Google</td>                        
                        <td>Aceptación de cookies en sitios web.</td>                        
                        <td>16 años</td>                        
                    </tr>   
                    <tr>
                        <td>NID</td>                        
                        <td>Google</td>                        
                        <td>Visualización de mapas mediante Google Maps.</td>                        
                        <td>6 meses</td>                        
                    </tr>   
                    <tr>
                        <td>Laravel_session</td>                        
                        <td>Sitio web</td>                        
                        <td>Permita la gestión de la sesión</td>                        
                        <td>2 horas</td>                        
                    </tr>                   
                </tbody>
            </table>          

            <h5>Gestión de cookies a través del navegador</h5>
            <p>Puede usted permitir, bloquear o eliminar las cookies instaladas en su equipo mediante la configuración de las opciones del navegador instalado en su ordenador. Por favor, lea atentamente la sección de ayuda de su navegador para conocer más acerca de cómo activar el “modo privado” o desbloquear determinadas cookies.</p>
            <ul> 
                <li><b>Edge / Internet Explorer</b>. <br/>
                    Puede consultar el <a href="https://support.microsoft.com/es-es/windows/eliminar-y-administrar-cookies-168dab11-0753-043d-7c16-ede5947fc64d">soporte de Microsoft</a> o la Ayuda del navegador.
                </li>
                <li><b>Chrome</b>. <br/>
                    Puede consultar el <a href="https://support.google.com/chrome/answer/95647?co=GENIE.Platform%3DDesktop&amp;hl=es">soporte de Google</a> o la Ayuda del navegador.
                </li>
                    <li><b>Firefox</b>. <br/>
                    Puede consultar el <a href="https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias">soporte de Mozilla</a> o la Ayuda del navegador.
                </li>
            </ul>  

            <p>
            <i>Asociación Cristiana de Recursos Educativos Nueva Generación</i> no asume ninguna responsabilidad por problemas legales o técnicos causados por el incumplimiento por parte del Usuario de las recomendaciones incluidas. Esta comunicación se realiza para el conocimiento y uso de los usuarios, por consiguiente, no debe utilizarse para ninguna otra finalidad.
            </p>          

            <h5>Actualizaciones y cambios en la Política de Cookies.
</h5>
            <p>
            <i>Asociación Cristiana de Recursos Educativos Nueva Generación</i> podrá modificar esta Política de Cookies en función de exigencias legislativas, reglamentarias, o con la finalidad de adaptar dicha política a las instrucciones dictadas por la Agencia Española de Protección de Datos, por ello se aconseja a los Usuarios que la visiten periódicamente.</p>
        </div>
    </div>  
@endsection