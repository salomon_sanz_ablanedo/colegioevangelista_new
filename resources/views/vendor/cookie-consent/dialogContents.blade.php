<div class="jscookieconsent fixed bottom-0 inset-x-0 pb-2" style="position: fixed; bottom:0px; z-index:999; background-color:#FFFF; max-width: 700px; border-radius: 3px 3px 0px 0px; font-size:12px !important; text-align:center; border: 2px solid #CCC;">
    <div class="max-w-7xl mx-auto px-6">
        <div class="p-2 rounded-lg bg-yellow-100">
            <div class="flex items-center justify-between flex-wrap">
                <div class="w-0 flex-1 items-center hidden md:inline">
                    <p class="ml-3 text-black cookie-consent__message">
                        Esta web utiliza cookies propias y de terceros, para facilitarle su navegación por el sitio web.
Puede aceptar o rechazar las cookies que no son estrictamente necesarias. Para más detalles
consulte nuestra <a href="{{ route('web.cookies') }}">Política de Cookies</a>.
                    </p>
                </div>
                <div class="mt-2 flex-shrink-0 w-full sm:mt-0 sm:w-auto">
                    <button class="js-cookie-consent-disagree cookie-consent__agree cursor-pointer flex items-center justify-center px-4 py-2 rounded-md text-sm font-medium text-yellow-800 bg-yellow-400 hover:bg-yellow-300">Rechazar</button>
                    <button class="js-cookie-consent-agree cookie-consent__agree cursor-pointer flex items-center justify-center px-4 py-2 rounded-md text-sm font-medium text-yellow-800 bg-yellow-400 hover:bg-yellow-300">
                        Aceptar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
