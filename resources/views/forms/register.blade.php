@inject('DateUtils', 'App\Utils\DateUtils')

@extends('layouts.master_forms')

@section('main')

<form id="form_registration" method="POST" action="{{ route('form_register.submit', $registration->public_id) }}">

<div id="form_principal">
    <div class="row mb-3">
        <div class="col-12 mini-panel p-5">
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-3 col-form-label col-form-label-sm">Alumno:</label>
                <div class="col-sm-9">
                    <span>{{ $alumn->surname }}, {{ $alumn->name }}</span>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-3 col-form-label col-form-label-sm">Curso de inscripción:</label>
                <div class="col-sm-9">
                    <span>{{ $registration->grade->getCompleteNameLongAttribute()  }}</span>
                </div>
            </div>
            @if ($registration->status == 'SIN RELLENAR')

            <div class="form-group row">
                <label for="staticEmail" class="col-sm-3 col-form-label col-form-label-sm">Precio de enseñanza:</label>            
                <div class="col-sm-9">
                    {{ $alumn->price_with_scholarship ?? $registration->grade->cycle->education_price }}&euro; mensuales <span class="small text-muted">(cuotas entre septiembre y junio incluidos; se abonarán del 1 al 5 de cada mes).</span>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-3 col-form-label col-form-label-sm">Precio de matrícula:</label>
                <div class="col-sm-9">
                    @if ($registration->price === null)
                        {{ $alumn->price_with_scholarship ?? $registration->grade->cycle->registration_price }}&euro;
                    @else
                        {{ $registration->price + $registration->price_subjects }}&euro;
                        @if (!empty($registration->price_subjects))
                        <br>
                        <small class="text-muted">({{ $registration->price }}&euro; de precio matrícula + {{ $registration->price_subjects }}&euro; por asignatura/s suspensas. Son 25&euro; por cada una suspensa)</small>
                        @endif
                    @endif
                </div>
            </div>
            @endif
            @if ($registration->status != 'SIN RELLENAR')
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-3 col-form-label col-form-label-sm">Estado de matrícula:</label>
                <div class="col-sm-9">
                    @if ($registration->status == 'PENDIENTE REVISION')
                    <span class="badge badge-warning">En revisión</span><br/>
                    <small class="text-muted">La inscripción está a la espera de revisión y validación por parte del centro.</small>
                    @else
                    <span class="badge badge-success">Validada</span><br/>
                    <small class="text-muted">La inscripción ha sido revisada por el centro y validada</small>
                    @endif
                </div>
            </div>
            @endif
        </div>
    </div>
@if ($registration->status != 'SIN RELLENAR')
</div>
</form>
    @endsection
@else
<div class="row mb-3">
    <div class="col-12 mini-panel p-5">
            <h4 class="mb-4">Datos del Alumno</h4>
            @if (empty($alumn->name) || empty($alumn->surname))
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-3 col-form-label col-form-label-sm">Nombre</label>
                <div class="col-sm-9">
                    <div class="input-group">
                        <input type="text" class="form-control form-control-sm" value="{{ $alumn->name }}" name="name">
                        <label for="" class="col-sm-3 col-form-label col-form-label-sm">Apellidos</label>
                        <input type="text" class="form-control form-control-sm" value="{{ $alumn->surname }}" name="surname">
                    </div>
                </div>
            </div>
            @endif

            @empty($alumn->birthday)
            <div class="form-group row">
                <label for="" class="col-sm-3 col-form-label col-form-label-sm">Fecha de nacimiento</label>
                <div class="col-sm-9">
                    <div class="input-group">
                        <select class="form-control text-center" required name="birthday_day">
                            @for($a=1; $a<=31; $a++)
                            <option value="{{ $a }}">{{ $a }}</option>
                            @endfor
                        </select>
                        &nbsp;
                        <select class="form-control text-center" required name="birthday_month">
                            @for($b=1; $b<=12; $b++)
                            <option value="{{ $b }}">{{ $DateUtils::monthName($b) }}</option>
                            @endfor
                        </select>
                        &nbsp;
                        <input type="number" step="1" min="2000" max="2017" class="form-control text-center" value="" required name="birthday_year"/>
                    </div>
                </div>
            </div> 
            @endempty

            <div class="form-group row">
                <label for="" class="col-sm-3 col-form-label col-form-label-sm">Lugar de nacimiento</label>
                <div class="col-sm-9">
                    <div class="input-group">
                        <input type="text" class="form-control form-control-sm" value="{{ $alumn->birthplace }}" name="birthplace" required>
                        <label for="" class="col-sm-3 col-form-label col-form-label-sm">Nacionalidad</label>
                        <input type="text" class="form-control form-control-sm" value="{{ $alumn->nacionality }}" name="nacionality"required>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="" class="col-sm-3 col-form-label col-form-label-sm">Domicilio</label>
                <div class="col-sm-9">
                    <textarea class="form-control form-control-sm small" style="font-size: 10pt;" required name="address">{{ $alumn->address }}</textarea>
                </div>
            </div>
            @if (!isset($alumn->brothers_total) || !isset($alumn->brothers_index))
            <div class="form-group row">
                <label for="" class="col-sm-3 col-form-label col-form-label-sm">
                    Hermanos total<br/>
                    <small class="text-muted text-italic">Incluido el alumno</small>
                </label>
                
                <div class="col-sm-9">
                    <div class="input-group">
                        <input type="number" min="0" step="1" class="form-control form-control-sm" name="brothers_total" value="{{ $alumn->brothers_total }}">
                         <label for="" class="col-sm-3 col-form-label col-form-label-sm">Lugar que ocupa</label>
                        <input type="number" min="0" step="1" class="form-control form-control-sm" name="brothers_index" value="{{ $alumn->brothers_index }}">
                    </div>
                </div>
            </div>
            @endif

            @foreach($contacts as $key=>$contact)
            <hr/>
            <fieldset>
                <legend class="h5">Padre, Madre o Tutor {{ $key+1 }}</small></legend><br/>
            @foreach($contact as $field)
            <div class="form-group row">
                <label for="" class="col-sm-3 col-form-label col-form-label-sm">{{ $field['label'] }}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control form-control-sm" name="contact[{{ $key }}][{{ $field['name'] }}]" value="{{ $field['value'] ?? '' }}" {{ $key == 0 ? 'required' : '' }} placeholder="{{ $field['placeholder'] ?? '' }}">
                </div>
            </div>
            @endforeach
            </fieldset>
            @endforeach
    </div>
</div>

<div class="row mb-3">
    <div class="col-12 mini-panel p-5">
        <h4 class="mb-4">Documentación a adjuntar</h4>

        @foreach($registration->grade->cycle->docs as $document)

            <div class="row mt-3 mb-3" style="border:1px solid #CCC;">
                <div class="col-12 px-3 pt-4">
                    <h5 class="card-title">{{ $document->docType->name }}</h5>
                    <p class="card-text">
                        {!! $document->text_info !!}
                    </p>
                </div>
                <div class="col-12 p-2" v-if="!required_docs.type_{{ $document->docType->id }}" style="height:40px; background-color:#fff1c6; overflow:hidden;">
                    <input type="file" accept="image/*,application/pdf,application/zip,application/docx" {{ $document->required_type == 'MANDATORY' ? 'required' : '' }} @change="_upload(event, '{{ $document->docType->id }}')"/>
                </div>
                <div class="col-12 p-2" style="background-color:#FFFFFF; height:40px;" v-else-if="required_docs.type_{{ $document->docType->id }} == 'loading'">
                    Subiendo...
                    <div id="type_{{ $document->docType->id }}_loading" class="bg-info" style="height: 4px; position:absolute;"></div>
                </div>
                <div class="col-12 p-2" style="background-color:#c7ffd4; height:40px;" v-else>
                    <a class="small" href="javascript:void(0);" @click="required_docs.type_{{ $document->docType->id }} = null;">elegir otro archivo</a>
                    <svg class="svg-inline--fa fa-check fa-w-16 fa-3x float-right" style="color: rgb(80, 224, 80); margin-top: -24px;" aria-hidden="true" focusable="false" data-prefix="fa" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg>
                </div>
            </div>
        @endforeach
    </div>
</div>
</div>
<div class="row">
    <div class="col-12 mini-panel">
        <div class="row">
            <div class="col-12">
                <h4 class="card-title">Firma de conformidad</h4>
                <p class="card-text ml-3">
                    <input type="checkbox" id="check_data"> &nbsp;Declaro que los progenitores/tutores damos el consentimiento para el <a href="/documents/proteccion_datos.pdf" target="_blank">tratamiento de datos</a>.<br/>
                    <input type="checkbox" id="check_fe"> &nbsp;Declaro que he leído la <a href="/documents/declaracion_de_fe.docx" target="_blank">declaración de fe</a> y estoy de acuerdo.<br/>
                    <input type="checkbox" id="check_agree"> &nbsp;Declaro que los progenitores/tutores estamos conformes con esta solicitud<br/>
                    <input type="checkbox" id="check_resolution"> &nbsp;Declaro que los progenitores/tutores: o no están en situación de separación, divorcio o discrepancia, o si lo están aportan copia del convenio regulador o sentencia judicial.
                </p>
                <hr/>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-6">
                <span class="text-muted small">Firma del padre / tutor</span>

                <div style="border:1px solid #CCC;" class="mb-1">
                    <canvas style="width:336px; height:112px;" id="signature1"></canvas>
                </div>
                <button type="button" class="btn btn-small btn-secundary btn-outline-secondary p-1 float-right" style="font-size: 8pt;" onclick="signaturePad1.clear();">borrar</button>
            </div>
            <div class="col-6">
                <span class="text-muted small">Firma de la madre / tutora</span>
                <div style="border:1px solid #CCC;" class="mb-1">
                    <canvas style="width:336px; height:112px;" id="signature2"></canvas>
                </div>
                <button type="button" class="btn btn-small btn-secundary btn-outline-secondary p-1 float-right" style="font-size: 8pt;" onclick="signaturePad2.clear();">borrar</button>
            </div>
        </div>
    </div>
 </div>
<div class="row mt-5">
    <div class="col-12 mb-5">
        <button type="button" class="btn btn-primary form-control btn-lg" onclick="app.submit()">Enviar</button>
    </div>
</div>
</form>

@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script>

if (!window.fetch || !window.Promise)
{
    alert('¡Atención! Es posible que estés usando un navegador viejo incompatible. Te recomendamos que descargues un navegador actualizado: Google Chrome, Firefox, Edge...');
}

var required_docs = {};

@foreach($registration->grade->cycle->docs as $doc)
    required_docs['type_{{  $doc->docType->id  }}'] = '{{ $doc->docType->name }}';
    // {
    //     name     : '{{ $doc->docType->name }}',
    //     required : 
    // }
@endforeach

var signaturePad1;
var signaturePad2;

var app = new Vue({

    mounted : function(){
        this._loadDocs();
    },

    data : {
        required_docs : (function(r){
            var result = {}
            for (var prop in r)
                result[prop] = null;
            return result;
        })(required_docs)
    },

    methods : {

        submit : function()
        {
            var errors = [];

            // for(key in this.required_docs)
            // {
            //     if (!this.required_docs[key])
            //         errors.push(required_docs[key]);
            // }

            if (signaturePad1.isEmpty() && signaturePad2.isEmpty())
                errors.push('Es necesaria la firma de al menos un padre/madre/tutor');

            if (!$('#check_data').prop('checked') ||
                !$('#check_fe').prop('checked') ||
                !$('#check_agree').prop('checked') || 
                !$('#check_resolution').prop('checked')
            )
                errors.push('Falta marcar algunas de las casillas de conformidad con las condiciones');

            if (errors.length > 0)
            {
                alert('Falta adjuntar los siguientes documentos: \n\n- ' + errors.join('\n- '));
                return;
            }

            if (!$('#form_registration')[0].checkValidity())
                $('#form_registration')[0].reportValidity();
            else
                $('#form_registration').submit();
        },

        _loadDocs : function(type_id)
        {
            axios.get('/api/registration/{{ $registration->public_id }}/docs')
                .then(function(result){
                    if (result.data.length)
                    {
                        result.data.forEach(function(doc){
                            
                            this.setDocUploaded(doc.type_id);

                            if (doc.preview)
                            {
                                if (doc.type_id == 2 && signaturePad1)
                                {
                                    signaturePad1.fromDataURL("data:image/png;base64,"+doc.preview);
                                }
                                else if (doc.type_id == 3 && signaturePad2)
                                {
                                    signaturePad2.fromDataURL("data:image/png;base64,"+doc.preview);
                                }
                            }
                        }.bind(this));

                        this.$forceUpdate();
                    }
                }.bind(this))
        },

        setDocUploaded : function(type_id)
        {
            this.required_docs['type_'+type_id] =  'done';
        },

        _upload : function(event, type)
        {
            this.photo_loading = false;

            let formData = new FormData();
            formData.append('type', type);

            if (typeof event == 'object')
                formData.append('file', event.target.files[0]);
            else
            {
                // sign
                formData.append('file', event);
            }

            this.required_docs['type_'+type] = 'loading';

            axios.post(
                '/forms/register/{{ $registration->public_id }}/file', 
                formData,{
                    headers: { 'Content-Type': 'multipart/form-data'},
                    onUploadProgress: function(progressEvent) {
                        var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                        $('#type_'+type+'_loading').css('width', percentCompleted+'%');
                    }
                })
                .then(function(result){
                    this.setDocUploaded(result.data.type_id);
                }.bind(this))
                .catch(function(e){
                    this.required_docs['type_'+type] = null;
                    alert('Error al guardar');
                }.bind(this))
                .finally(function(){
                    //this.photo_loading = false;
                    this.$forceUpdate();
                }.bind(this));
        }
    }
});

$(document).ready(function(){
    setTimeout(function(){

        // var props = {
        //     minWidth : 0.4,
        //     maxWidth : 2,
        //     penColor : "#0b07f3",
        //     /*minDistance : 2,
        //     throttle : 5*/
        // };

        signaturePad1 = new SignaturePad($("#signature1")[0],  {
            minWidth : 0.2,
            maxWidth : 1.7,
            penColor : "#0b07f3",
            onEnd : function(e){
                app._upload(signaturePad1.toDataURL(), 2);
            }
        });
        
        signaturePad2 = new SignaturePad($("#signature2")[0], {
            minWidth : 0.2,
            maxWidth : 1.7,
            penColor : "#0b07f3",
            onEnd : function(e){
                app._upload(signaturePad2.toDataURL(), 3);
            }
        });

        resizeCanvas($("#signature1")[0], signaturePad1);
        resizeCanvas($("#signature2")[0], signaturePad2);
        app.$mount('#form_principal');
    },1000);
}.bind(this));

function resizeCanvas(canvas, signaturePad) {
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
    signaturePad.clear(); // otherwise isEmpty() might return incorrect value
}
</script>
@endpush
@endif