@extends('layouts.master_forms')

@section('main')
<form>
<div>
<div class="row">
    <div class="col-12 mini-panel p-5">

        <p>A continuación listamos la documentación necesaria que hay que aportar para realizar la preinscripción en Educación Secundaria Obligatoria</p>
        <p>
            Una vez completada, debe ser enviada por email a:<br><br/>
            <a href="mailto:administracion@colegioevangelista.com">administracion@colegioevangelista.com</a>
        </p>
        <p>Puedes descargarte la documentación y los diferentes formularios en el siguiente enlace:</p>
        <a class="btn btn-primary" href="/documents/ESO.zip">Descargar documentación</a>
    </div>
</div>
<div class="row mt-4">
    {{-- <h3 class="text-center">Documentación a adjuntar</h3> --}}
    <div class="col-12 mini-panel p-5">
        <div class="row">
            <div class="col-6">
                        <h5 class="card-title">Fotografía</h5>
                        <p class="card-text">
                            Es necesario adjuntar una fotografía del alumno tipo DNI:<br>
                        </p>
            </div>
            <div class="col-6">
                <ul>
                    <li>Con fondo blanco</li>
                    <li>Encuadrando desde el cuello sin cortar cabeza</li>
                    <li>En orientación vertical</li>
                </ul>
            </div>
        </div>
     </div>
</div>

<div class="row mt-4">
    {{-- <h3 class="text-center">Documentación a adjuntar</h3> --}}
    <div class="col-12 mini-panel p-5">
        <div class="row">
            <div class="col-12">
                <h5 class="card-title">Formulario de Inscripción Gobierno de Navarra</h5>
                <p class="card-text">Documento Incluido en la descarga. <br/>
                    Una vez rellenado hazle una foto para adjuntarlo (sólo se necesita la hoja 1 y 2).</p>
            </div>
        </div>
    </div>
</div>

<div class="row mt-4">
    {{-- <h3 class="text-center">Documentación a adjuntar</h3> --}}
    <div class="col-12 mini-panel p-5">
        <div class="row">
            <div class="col-12">
                <h5 class="card-title">Libro de familia</h5>
                <p class="card-text">Haz una fotografía a cada una de las páginas del libro de familia donde haya datos y adjúntalas en el email.</p>
            </div>
        </div>
    </div>
</div>

<div class="row mt-4">
    {{-- <h3 class="text-center">Documentación a adjuntar</h3> --}}
    <div class="col-12 mini-panel p-5">
        <div class="row">
            <div class="col-12">
                <h5 class="card-title">Declaración de fe</h5>
                <p class="card-text">Documento incluido en la descarga. Es necesario adjuntar una fotografía del documento firmado por ambos progenitores</p>
            </div>
        </div>
    </div>
</div>

<div class="row mt-4">
    {{-- <h3 class="text-center">Documentación a adjuntar</h3> --}}
    <div class="col-12 mini-panel p-5">
        <div class="row">
            <div class="col-12">
                <h5 class="card-title">Convenio regulador o sentencia judicial</h5>
                <p class="card-text">Sólo necesario en el caso de que los progenitores/tutores estén en situación de separación, divorcio o discrepancia. Realizar foto y adjuntarlo.</p>
            </div>
        </div>
    </div>
</div>

<div class="row mt-4">
    {{-- <h3 class="text-center">Documentación a adjuntar</h3> --}}
    <div class="col-12 mini-panel p-5">
        <div class="row">
            <div class="col-12">
                <h5 class="card-title">Ficha del alumno</h5>
                <p class="card-text">Documento incluido en la descarga. Descargalo y rellénalo. Una vez completado adjúntalo también en el email.</p>
            </div>
        </div>
    </div>
</div>

<div class="row mt-4">
    {{-- <h3 class="text-center">Documentación a adjuntar</h3> --}}
    <div class="col-12 mini-panel p-5">
        <div class="row">
            <div class="col-12">
                <h5 class="card-title">Historial Académico</h5>
                <p class="card-text">Para los alumnos nuevos, es necesario incluir el historial académico (deberás solicitarlo en el centro de origen).</p>
            </div>
        </div>
    </div>
</div>
</form>

@endsection
