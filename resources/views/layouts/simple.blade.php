@extends('layouts.master')

@push('css')
<style>
    body{
        background: linear-gradient(to bottom, rgba(92, 77, 66, 0.8) 0%, rgba(92, 77, 66, 0.8) 100%), url(/images/bg-masthead.jpg?4faf4a7…);
        background-position: center;
        background-repeat: repeat;
        background-attachment: scroll;
        background-size: cover;
    }
p {
    font-size:14px;
}
h5{
    margin-top:35px !important;    
}
.table_bank td{
    border-top:0px !important;
}

</style>
@endpush

@section('main')
        <!-- Masthead-->
        {{-- <header class="masthead">
           
            <br/>
        </header> --}}
         <div class="container mb-4" style="background-color:#FFFFFF; border-radius:7px; margin-top:5.5rem;">
            @yield('content')                
        </div>           
@endsection