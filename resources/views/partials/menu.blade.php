<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav" style="position:absolute;">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger text-center" href="{{ route('web.index') }}" style="font-size: 10pt;">
            {{-- <img src="{{ asset('img/logo_menu.png') }}" width="250"> --}}
            Asociación Cristiana de Recursos Educativos <br>&quot;Nueva Generación&quot;
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{ route('web.index') }}#matriculas">Información</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{ route('web.index') }}#vision">Visión</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{ route('web.index') }}#ubication">Ubicación</a></li>
                <li class="nav-item"><span class="badge badge-danger" style="position: absolute; margin-left: 70px; margin-top: -3px; font-size:8px;">nuevo</span><a class="nav-link js-scroll-trigger" href="{{ route('web.collaborate') }}">Colabora</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{ route('web.index') }}#contact">Contacta</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{ url('admin') }}">Admin</a></li>
            </ul>
        </div>
    </div>
</nav>