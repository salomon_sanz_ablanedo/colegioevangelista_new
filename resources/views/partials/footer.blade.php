<footer class="py-4">
    <div class="container">
        <div class="small text-center text-muted">
            {{-- Asociación Cristiana de Recursos Educativos &quot;Nueva Generación&quot;  - 2020
            |  --}}
            <a href="{{ route('web.privacy') }}">Política de privacidad</a> |
            <a href="{{ route('web.legal') }}">Aviso legal</a> |
            <a href="{{ route('web.cookies') }}">Política de Cookies</a>
        </div>
    </div>
</footer>