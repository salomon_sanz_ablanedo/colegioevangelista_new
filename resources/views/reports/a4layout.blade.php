<html>
	<head>
		<meta content="text/html; charset=UTF-8" http-equiv="content-type">
        <style type="text/css">
            * { 
                -moz-box-sizing: border-box; 
                -webkit-box-sizing: border-box; 
                box-sizing: border-box; 
            }
            html, body{
                margin:0px !important;
                padding:0px !important;
                width: 210mm;
                font-family:Arial;
                font-size: 12pt;
            }            
            
            div.page            
            {
                margin:0px;
                width:210mm; 
                height:297mm; 
                padding:5mm 10mm;
                /*
                display: inline-block;
                overflow: hidden;
                */
                page-break-after: always;
                page-break-inside: avoid;    
            }                       
		</style>
        @stack('css')
	</head>
	<body>
        @foreach($pages as $page)
            <div class="page">
                {!! $page !!}
            </div>
        @endforeach       
	</body>
</html>