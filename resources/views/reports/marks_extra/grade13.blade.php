 <table class="table" style="width:100%;">
            <tbody>
                <tr>
                    <td style="text-align:center;">
                        {{ $alumn->classroom->grade->number .' º '. $alumn->classroom->grade->cycle->short_name }}
                    </td>
                    <td style="text-align:center;">
                        CONSEJO ORIENTADOR <br/>(itinerario formativo aconsejable para el próximo curso)
                    </td>
                </tr>                
                <tr>
                    <td>
                        <input type="checkbox"/> Promociona
                    </td>
                    <td>
                        <input type="checkbox"/>Acceso a Bachiller<br/>
                        <input type="checkbox"/>Acceso a Formación Profesional<br/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox"/> Promociona automáticamente, por imposibilidad de repetir 
                    </td>
                    <td>
                        <input type="checkbox"/>Acceso a Bachiller<br/>
                        <input type="checkbox"/>Acceso a Formación Profesional<br/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox"/> No promociona 
                    </td>
                    <td>
                        <input type="checkbox"/> Acceso a Bachiller DC <br/>
                        <input type="checkbox"/> Repetición {{ $alumn->classroom->grade->number}}º ESO <br/>
                        <input type="checkbox"/> Repetición de {{ $alumn->classroom->grade->number}}º en PCA <br/>
                        <input type="checkbox"/> Acceso a CFGB <br/>
                    </td>
                </tr>

                {{-- <tr class="c21">
                    <td class="c0" colspan="1" rowspan="1"><p class="c11"><span class="c3">Tecnolog&iacute;a</span></p></td>
                    <td class="c26" colspan="1" rowspan="1"><p class="c2"><span class="c3"></span></p></td>
                    <td class="c22" colspan="1" rowspan="1"><p class="c2"><span class="c3"></span></p></td>
                    <td class="c4" colspan="1" rowspan="1"><p class="c2"><span class="c3"></span></p></td
                        <td class="c17" colspan="1" rowspan="1"><p class="c2"><span class="c3"></span></p></td>
                        <td class="c9" colspan="1" rowspan="1"><p class="c2"><span class="c3"></span></p></td>
                </tr> --}}
            </tbody>
        </table>