    <style type="text/css">

        h1{
            display: inline;
        }
        
        .small{
            font-size: 8pt;
        }

        table.table{
            border-spacing: 0;
            border-collapse: collapse;
            vertical-align: top;
        }
        table.table td, table.table th
        {
            vertical-align: middle;
            padding: 5pt 5pt 5pt 5pt;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-width: 1pt;
            border-bottom-width: 1pt;
            border-bottom-color: #000000;
            border-left-color: #000000;
            border-right-color: #000000;
            border-top-color: #000000;
            border-right-style: solid;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-style: solid;
            font-weight: 400;
        }
        
        .c34{border-right-style:solid;padding:5pt 5pt 5pt 5pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:131.2pt;border-top-color:#000000;border-bottom-style:solid}
        .c22{border-right-style:solid;padding:5pt 5pt 5pt 5pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:71.2pt;border-top-color:#000000;border-bottom-style:solid}
        .c24{border-right-style:solid;padding:5pt 5pt 5pt 5pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:77.2pt;border-top-color:#000000;border-bottom-style:solid}
        .c20{border-right-style:solid;padding:5pt 5pt 5pt 5pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:79.5pt;border-top-color:#000000;border-bottom-style:solid}
        .c26{border-right-style:solid;padding:5pt 5pt 5pt 5pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:64.5pt;border-top-color:#000000;border-bottom-style:solid}
        .c36{border-right-style:solid;padding:5pt 5pt 5pt 5pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:171pt;border-top-color:#000000;border-bottom-style:solid}
        .c17{border-right-style:solid;padding:5pt 5pt 5pt 5pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:69pt;border-top-color:#000000;border-bottom-style:solid}
        .c28{border-right-style:solid;padding:5pt 5pt 5pt 5pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:93pt;border-top-color:#000000;border-bottom-style:solid}
        .c5{border-right-style:solid;padding:5pt 5pt 5pt 5pt;border-bottom-color:#b7b7b7;border-top-width:0pt;border-right-width:0pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:0pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;border-top-color:#000000;border-bottom-style:solid}
        .c13{border-right-style:solid;padding:5pt 5pt 5pt 5pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:156.8pt;border-top-color:#000000;border-bottom-style:solid}
        .c4{border-right-style:solid;padding:5pt 5pt 5pt 5pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:67.5pt;border-top-color:#000000;border-bottom-style:solid}
        .c0{border-right-style:solid;padding:5pt 5pt 5pt 5pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:162pt;border-top-color:#000000;border-bottom-style:solid}
        .c3{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:11pt;font-family:"Arial";font-style:normal}
        .c15{
            color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-family:"Arial";font-style:normal
        }

        .c2{
            padding-top:0pt;
            padding-bottom:0pt;
            line-height:1.0;
            text-align:center; 
            height:11pt
        }
        .c33{color:#000000;text-decoration:none;vertical-align:baseline;font-family:"Arial";font-style:normal}
        .c31{padding-top:0pt;padding-bottom:0pt;line-height:1.15;text-align:left}
        .c11{padding-top:0pt;padding-bottom:0pt;line-height:1.0;text-align:left}
        .c1{padding-top:0pt;padding-bottom:0pt;line-height:1.0;text-align:center}
        .c25{border-spacing:0;border-collapse:collapse;margin-right:auto}
        .c16{padding-top:0pt;padding-bottom:0pt;line-height:1.0;text-align:right}
        .c38{padding-top:0pt;padding-bottom:0pt;line-height:1.15;text-align:right}
        .c32{border-spacing:0;border-collapse:collapse;margin-right:auto}
        /* .c39{background-color:#ffffff;max-width:496.4pt;padding:0 49.5pt 72pt 49.5pt} */
        .c23{margin-left:-28.5pt;margin-right:-13.5pt}
        .c10{orphans:2;widows:2}
        .c12{font-weight:700}
        .c19{height:74pt}
        .c6{height:0pt}
        .c7{font-size:8pt}
        .c27{font-size:7pt}
        .c21{height:24pt}
        .c18{height:20pt}
        .c40{font-size:14pt}
        .c14{height:3pt}
        .c29{font-size:12pt}
        .c30{height:11pt}
        .c35{font-size:10pt}
        .title{padding-top:0pt;color:#000000;font-size:26pt;padding-bottom:3pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}
        .subtitle{padding-top:0pt;color:#666666;font-size:15pt;padding-bottom:16pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}li{color:#000000;font-size:11pt;font-family:"Arial"}p{margin:0;color:#000000;font-size:11pt;font-family:"Arial"}h1{padding-top:20pt;color:#000000;font-size:20pt;padding-bottom:6pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h2{padding-top:18pt;color:#000000;font-size:16pt;padding-bottom:6pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h3{padding-top:16pt;color:#434343;font-size:14pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h4{padding-top:14pt;color:#666666;font-size:12pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h5{padding-top:12pt;color:#666666;font-size:11pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h6{padding-top:12pt;color:#666666;font-size:11pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;font-style:italic;orphans:2;widows:2;text-align:left}
    </style>    
            
        @include('reports.partials.header_with_alumn', $alumn_header_data)
        <br/>
        
        <table class="table" style="width:100%;">
            <tbody>
                <tr>
                    <td class="c0" colspan="1" rowspan="1"><p class="c1"><span class="c3">&Aacute;reas de conocimiento</span></p></td>
                    <td class="c26" colspan="1" rowspan="1"><p class="c1"><span class="c3">1&ordf; Ev<br>CAL</span></p></td>
                    <td class="c22" colspan="1" rowspan="1"><p class="c1"><span class="c3">2&ordf; Ev<br>CAL</span></p></td>
                    <td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c3">3&ordf; Ev<br>CAL</span></p></td>
                    <td class="c34" colspan="2" rowspan="1"><p class="c1"><span class="c3">Ev Final<br>CAL</span></p></td>
                </tr>
                @foreach($subjects as $subject)
                    @if (!empty($subject->parent_area_id) || !$subject->valuable)
                        @continue
                    @endif
                <tr>
                    <td class="c0" colspan="1" rowspan="1"><p class="c11"><span class="c3">{{ $subject->name }}</span></p></td>
                    @for ($i = 0; $i < 4; $i++)
                        <td class="c26" colspan="1" rowspan="1"><p class="c2"><span class="c3">{{ $subject->marks[$i] }}</span></p></td>
                    @endfor
                </tr>
                @endforeach
                {{-- <tr class="c21">
                    <td class="c0" colspan="1" rowspan="1"><p class="c11"><span class="c3">Tecnolog&iacute;a</span></p></td>
                    <td class="c26" colspan="1" rowspan="1"><p class="c2"><span class="c3"></span></p></td>
                    <td class="c22" colspan="1" rowspan="1"><p class="c2"><span class="c3"></span></p></td>
                    <td class="c4" colspan="1" rowspan="1"><p class="c2"><span class="c3"></span></p></td
                        <td class="c17" colspan="1" rowspan="1"><p class="c2"><span class="c3"></span></p></td>
                        <td class="c9" colspan="1" rowspan="1"><p class="c2"><span class="c3"></span></p></td>
                </tr> --}}
            </tbody>
        </table>

        <br/>    
        <table>
            <tbody><tr>
                <td style="width:50%">
                    <span class="c7 c12">CAL</span><span class="c7">: Calificación, </span><span class="c12 c7">REC</span><span class="c7">: Recuperación, </span><span class="c12 c7">MA</span><span class="c7">: Medidas adoptadas<br></span><span class="c12 c7">SB</span><span class="c7">: Sobresaliente, </span><span class="c12 c7">NT</span><span class="c7">: Notable, </span><span class="c12 c7">BI</span><span class="c7">: Bien, </span><span class="c12 c7">SU</span><span class="c7">: Suficiente, </span><span class="c12 c7">IN</span><span class="c7">: Insuficiente, </span>
                    <span class="c12 c7">NE</span><span class="c7">: No Evaluado </span>
                </td>
                <td>
                    <table class="table small">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Justificadas</th>
                                <th>No Justificadas</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Retraso
                                </td>
                                <td style="text-align:center;">
                                    {{ $absences['delay'][1] }}
                                </td>
                                <td style="text-align:center;">
                                    {{ $absences['delay'][0] }}
                                </td>
                            </tr>
                            <tr class="c6">
                                <td colspan="1" rowspan="1">Ausencia</td>
                                <td colspan="1" rowspan="1" style="text-align:center;">
                                    {{ $absences['absences'][1] }}
                                </td>
                                <td colspan="1" rowspan="1" style="text-align:center;">
                                    {{ $absences['absences'][0] }}
                                </td>                                
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
        </table>
        
        <div class="observations" style="height:88mm; overflow:hidden;">
            
            Observaciones del tutor
            <div style="height:20mm; width:100%; border:1px solid #000; padding:2mm;">
                <i>{{ $comments ?? '' }}</i>
            </div>
            <br/>
            Observaciones de asignaturas
            <div style="height:53mm; width:100%; border:1px solid #000; font-size:10pt; padding:2mm;">
                @foreach($subjects as $subject)
                @if (!empty($subject->comments))
                <i class="small"><b>{{ $subject->name }}: </b> {{ $subject->comments }}</i>
                <br>
                @endif
            @endforeach
            </div>
        </div>
        <p class="c1 c10 c23" style="margin-top:2mm;">
            <span class="c27">Cortar por la l&iacute;nea de puntos y entregar en el centro<br></span>
            <span class="c40">&#9985;</span>
            <span class="c7">&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;
            </span>
        </p>
        <p class="c11 c10">
            <span class="c12 c35">{{ $alumn->surname }}, {{ $alumn->name }}</span>
            <span class="c7">&nbsp;- </span>
            <span class="c15 c7">{{ $alumn->classroom->grade->number }}º {{ $alumn->classroom->grade->cycle->name }} &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;{{ $mark_type->name }}, {{ $alumn->classroom->promotion->name }}<br><br><br>Fecha: ______ de _______________________ de ________ &nbsp; &nbsp; &nbsp; &nbsp;<br> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <br><br>Firma del tutor &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Firma del Padre/Madre/Tutor</span>
        </p>
        </div>
       
        @if (!empty($areas)) 
        <div class="page">
            @include('reports.partials.header_with_alumn', $alumn_header_data)
            <br/>
            <h2>Aspectos evaluados por asignatura</h2>
            <br/>
            <table class="table" style="width:100%;">
                <tr>
                    <td style="width:40mm;"></td>
                    @foreach ($areas as $area_id)
                    <td class="small" style="text-align:center; width:20mm; word-wrap: break-word;">{{ $all_areas[$area_id] }}</td>
                    @endforeach
                </tr>
                @foreach($subjects as $s)
                    @if (empty($s->areas) || !$s->valuable)
                        @continue
                    @endif
                <tr>
                    <td>{{ $s->name }}</td>
                    @foreach($areas as $value)
                    <td style="text-align:center;">{{ $s->areas[$value] ?? '' }}</td>
                    @endforeach

                </tr>
                @endforeach

            </table>
        </div>
        @endif