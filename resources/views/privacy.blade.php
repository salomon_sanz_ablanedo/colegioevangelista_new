@extends('layouts.simple')

@section('content')
    <div class="row align-items-center justify-content-center text-center pt-5">
        <div class="col-lg-12 align-self-start ">
        {{-- <img src="{{ asset('img/logo_escudo.png') }}" width="150" class="mb-3"/> --}}
            <h1 class="text-uppercase font-weight-bold">Política de privacidad</h1>
            <hr class="divider my-4" />
        </div>
        {{-- <div class="col-lg-12 align-self-baseline">
            <p class="font-weight-light">información</p>
            <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>            
        </div> --}}
        <div class="py-2 px-5 text-muted small text-left">

            <p>A través de este sitio web no se recaban datos de carácter personal de los usuarios sin su conocimiento, ni se ceden a terceros.</p>
            <p>El portal del que es titular Asociación Cristiana de Recursos Educativos Nueva Generación contiene enlaces a sitios web de terceros, cuyas políticas de privacidad son ajenas a la de la entidad. Al acceder a tales sitios web usted puede decidir si acepta sus políticas de privacidad y de cookies. Con carácter general, si navega por internet usted puede aceptar o rechazar las cookies de terceros desde las opciones de configuración de su navegador.</p>
            <h5>Información básica sobre protección de datos</h5>
            <p>A continuación, le informamos sobre la política de protección de datos de Asociación Cristiana de Recursos Educativos Nueva Generación.</p>
            <h5>Responsable del tratamiento</h5>
            <p>Los datos de carácter personal que se pudieran recabar directamente del interesado serán tratados de forma confidencial y quedarán incorporados a la correspondiente actividad de tratamiento titularidad de Asociación Cristiana de Recursos Educativos Nueva Generación.</p>
            <h5>Finalidad</h5>
            <p>La finalidad del tratamiento de los datos se detalla en los formularios de contacto a través de los cuales se recaban los datos personales.</p>
            <ul>
                <li>prestación del servicio</li>
                <li>cumplimiento de obligaciones legales</li>
            </ul>

            <p>La comunicación de sus datos básicos de contacto es imprescindible para que le contactemos y respondamos a su solicitud o consulta.</p>
            <h5>Legitimación o base jurídica</h5>
            <p>Únicamente trataremos sus datos personales en base al consentimiento prestado por usted mediante una declaración o una clara acción afirmativa.</p>
            <h5>Conservación de datos</h5>
            <p>Los datos personales proporcionados se conservarán durante el tiempo necesario para cumplir con la finalidad para la que se recaban y para determinar las posibles responsabilidades que se pudieran derivar de la finalidad.</p>
            <h5>Cesión de datos</h5>
            <p>Con carácter general no se comunicarán los datos personales a terceros, salvo obligación legal, entre las que pueden estar las comunicaciones a los Jueces y Tribunales.</p>
            <p>Transferencias internacionales de datos: no se prevén.</p>
            <h5>Derechos de los interesados</h5>
            <p>Puede retirar el consentimiento prestado en cualquier momento y puede ejercer sus derechos de acceso, rectificación, supresión y portabilidad de sus datos, de limitación y oposición a su tratamiento, ante el responsable del tratamiento, enviando un correo electrónico a administracion@colegioevangelista.com </p>
            <h5>Delegado de protección de datos</h5>
            <p>El delegado de protección de datos es la persona física o jurídica que asesora al responsable o al encargado del tratamiento acerca de las obligaciones del RGPD y demás normativa aplicable en protección de datos, y supervisa el cumplimiento de aquélla y de las políticas del responsable o encargado de tratamiento, además de actuar como punto de contacto de la AEPD para cuestiones relativas al tratamiento de datos personales.</p>
            <p>Nuestro delegado de protección de datos es Federación de Entidades Religiosas Evangélicas de España, y puede contactarle para conocer más acerca del tratamiento de sus datos personales por esta entidad, en su domicilio en Calle Pablo Serrano, 9 posterior, de Madrid (28043), o en la dirección de correo electrónico protecciondatos@ferede.org.</p>
        </div>
    </div>  
@endsection