<?php

use Illuminate\Support\Facades\Route;

/* 
use Illuminate\Support\Facades\Auth;
Auth::loginUsingId(1, true); 
*/

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::view('admin/login', 'login');

Route::middleware(['site'])->group(function () {
    Route::get('forms/preregister_info', 'FormsController@getPreRegisterInfo');
    Route::get('forms/register/{id}', ['as' => 'form_register.form', 'uses' => 'FormsController@getRegister']);
    Route::any('forms/register/{id}/file', 'FormsController@postFile');
    Route::post('forms/register/{id}/submit', ['as' => 'form_register.submit', 'uses' => 'FormsController@postSubmit']);
    Route::get('/colabora', ['as'=> 'web.collaborate', 'uses'=>'WebController@getCollaborate']);
    Route::get('/aviso-legal', ['as'=> 'web.legal', 'uses'=>'WebController@getLegal']);
    Route::get('/politica-de-privacidad', ['as'=> 'web.privacy', 'uses'=>'WebController@getPrivacy']);
    Route::get('/politica-de-cookies', ['as'=> 'web.cookies', 'uses'=>'WebController@getCookies']);
    Route::get('/', ['as'=> 'web.index', 'uses'=>'WebController@getIndex']);
});

/* 
Route::get('/migratecontacts', function(){
    $alumns = \App\Models\Alumn::all();

    foreach($alumns as $alumn)
    {
        //$contacts = json_decode($alumn->contacts);

        //dd($alumn->contacts);

        foreach($alumn->contacts as $c)
        {
            $contact = new \App\Models\AlumnContact();            
            $contact->alumn_id = $alumn->id;            
            $contact->name = $c['name'] ?? null;       
            $contact->email = $c['email'] ?? null;
            $contact->phone = $c['phone'] ?? null;
            $contact->address = $c['address'] ?? null;
            $contact->relation = $c['relation'] ?? null;    
            $contact->nacionality = $c['nacionality'] ?? null;    
            $contact->dni = $c['dni'] ?? null;    
            $contact->civil_state = $c['civil_state'] ?? null;
            $contact->job = $c['job'] ?? null;    
            $contact->save();
        }
    }
});
 */