<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Alumn;
use App\Models\AlumnDoc;
use App\Models\Registration;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/registration/{id}/docs', function ($id, Request $request)
{
    $registration = Registration::withoutGlobalScope('promotion')
                                ->where('public_id', $id)->first();

    $docs = AlumnDoc::where('alumn_id', $registration->alumn_id);

    $result = [];

    if ($request->has('type'))
        $docs->where('type_id',$request->get('type'));
    
    $docs_result = $docs->get();

    if (count($docs_result) > 0)
    {
        foreach($docs_result as &$doc)
        {
            if (in_array($doc->type_id, [2,3]))
                $doc->preview = base64_encode(file_get_contents(storage_path('app/public/'.$doc->path)));
        }
    }

    return $docs_result->toJson();
});
